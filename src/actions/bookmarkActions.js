import Parse from 'parse'

export function getBoookmarks(){
  return function (dispatch){
    const Bookmarks = Parse.Object.extend('Bookmarks')
    const queryBookmarks = new Parse.Query(Bookmarks)
    const currentUser = Parse.User.current()
    if(currentUser){
      queryBookmarks.equalTo('user',currentUser)
      queryBookmarks.find({
        success(bookmarks){
          dispatch({
            type:"GET_BOOKMARKS",
            payload:bookmarks
          })
        },
        error(err){
          dispatch({
            type:"ERROR_GETTING_BOOKMARKS",
            payload:err.message
          })
        }
      })
    }
  }
}

export function guardarNuevoBookmark(bookmark){
  return function(dispatch){
    const Bookmarks = Parse.Object.extend('Bookmarks')
    const newBookmark = new Bookmarks()
    if(Parse.User.current()){
      newBookmark.set('user',Parse.User.current())
      newBookmark.set('titulo',bookmark.titulo)
      newBookmark.set('extent',bookmark.extent)
      newBookmark.save(null,{
        success(bookmark){
          dispatch({
            type:"SAVE_NEW_BOOKMARK",
            payload:bookmark
          })
          dispatch({
            type:"SNACKBAR_ON",
            payload:"Bookmar Guardado"
          })
        },
        error(bookmark,err){
          dispatch({
            type:"ERROR_SAVING_NEW_BOOKMARK",
            payload:err.message
          })
          dispatch({
            type:"SNACKBAR",
            payload:"Error en la creacion del bookmark"
          })
        }
      })
    }else{
    }
  }
}

export function borrarBookmark(bookmark){
  return function (dispatch) {
    bookmark.destroy({
      success(bookmark){
        dispatch({
          type:"DELETE_BOOKMARK",
          payload:bookmark
        })
      },
      error(bookmark,err){
        dispatch({
          type:"ERROR_DELETING_BOOKMARK",
          payload:err.message
        })
      }
    })
  }
}

export function editarBookmark(bookmark){
  return function (dispatch) {
    bookmark.save(null,{
      success(bookmark){
        dispatch({
          type:"EDITAR_BOOKMARK",
          payload:bookmark
        })
      },
      error(bookmark,err){
        dispatch({
          type:"ERROR_EDITAR_BOOKMARK",
          payload:err.message
        })

      }
    })
  }
}
