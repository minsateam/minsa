export function closeTable(layerid){
    return {
        type: "CLOSE_TABLE",
        payload:layerid

    }
}
export function openTable(layer){
    return {
        type:"OPEN_TABLE",
        payload:layer
    }
}
export function updateTable(layer){
    return {
        type:"UPDATE_TABLE",
        payload:layer
    }
}
export function setLayerTable(layer){
    return {
        type:"SET_LAYER_TABLE",
        payload:layer
    }
}