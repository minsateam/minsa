import axios from 'axios'
import ol from 'openlayers'

export function setMap(map) {
    return {
        type: "SET_MAP",
        payload: map
    }
}
export function changeLayers() {
    return {
        type: "CHANGE_LAYERS"
    }
}

export function moverLayerArriba(layer) {
    return {
        type: "MOVER_LAYER_ARRIBA",
        payload: layer
    }
}
export function moverLayerAbajo(layer) {
    return {
        type: "MOVER_LAYER_ABAJO",
        payload: layer
    }
}
export function getCapabilities() {

    return function (dispatch) {
        const url = "http://gisnovomap.com/geoserver/wms"
        const params = {
            service: "wms",
            version: "1.3.0",
            request: "GetCapabilities",
            namespace: "MINSA"
        }
        const auth = {
            username: "MINSA_Admin",
            password: "gj78rt96"
        }
        const auth2 = {
            username: "gliberte",
            password: "bDT-zeY-cn8-APG"
        }
        const config = {
            url: url,
            params: params
        }
        const format = new ol.format.WMSCapabilities()
        const result = axios(config)
            .then((response) => {
                const resultado = format.read(response.data)
                const capability = resultado.Capability

                dispatch({
                    type:"GET_CAPABILITIES",
                    payload:capability.Layer.Layer
                })
            })
            .catch((err) => {
                console.log(err)
            })
    }
}
export function setLayerResalto(layer){
    return {
        type:"SET_LAYERRESALTO",
        payload:layer
    }
}
export function setFeatureResaltado(feature){
    return {
        type:"SET_FEATURE_RESALTADO",
        payload:feature
    }
}
export function setSelectInteraction(interaction){
    return {
        type:"SET_SELECTINTERACTION",
        payload:interaction
    }
}
export function addSelectedFeature(feature){
    return {
        type:"ADD_SELECTED_FEATURE",
        payload:feature
    }
}
export function removeSelectedFeature(feature){
    return {
        type:"REMOVE_SELECTED_FEATURE",
        payload:feature
    }
}
export function spinerOn(){
    return {
        type:"SPINER_ON"
    }
}
export function spinerOff(){
    return {
        type:"SPINER_OFF"
    }
}
export function addSnap(snap){
    return {
        type:"ADD_SNAP",
        payload:snap
    }
}
