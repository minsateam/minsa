import axios from 'axios'
import {spinerOff,spinerOn} from './mapActions'

export function addResult(feature, typeName, value) {
    return {
        type: "ADD_RESULT",
        payload: {
            feature: feature,
            value: value,
            typeName: typeName
        }
    }
}

export function clearResults() {
    return {
        type: "CLEAR_RESULTS"
    }
}

export function endSearch(state) {
    return {
        type: "END_SEARCH",
        payload: state
    }
}


