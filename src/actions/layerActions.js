import axios from 'axios'
export function toggleAtributes(){
    return {
        type:"TOGGLE_ATRIBUTES"
    }
}
export function showFeatureInfoWms(featureUrl){
    return function (dispatch){
        dispatch({
            type:"FEATURE_INFO",
            payload:axios.get(featureUrl)
        })
    }
}
export function closeFeatureInfoWms(){
    return {
        type:"CLOSE_FEATURE_INFO"
    }
}