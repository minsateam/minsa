const InitialDialogContainer = ()=>(<div>Sin contenido</div>)
export function isEditing(stat){
    return {
        type:"IS_EDITING",
        payload:stat
    }
}
export function openFormNuevoUsuario(){
    return {
        type:"OPEN_FORM_NUEVOUSUARIO"
    }
}
export function openFormBookmarks() {
  return {
    type:"OPEN_FORM_BOOKMARKS"
  }
}

export function snackBarOn(mensaje){
  return {
    type:"SNACKBAR_ON",
    payload:mensaje
  }
}
export function snackBarOff(){
  return {
    type:"SNACKBAR_OFF"
  }
}

export function dialogContainerOpen(component,props,title){
  return {
    type:"DIALOGCONTAINER_OPEN",
    payload:{
      isOpen:true,
      component:component,
      propiedades:props,
      title:title
    }

  }
}
export function dialogContainerClose(){
  return {
    type:"DIALOGCONTAINER_CLOSE",
    payload:{
      isOpen:false
    }
  }
}