import React from 'react'
const InitialDialogContainer = ()=>(<div>Sin contenido</div>)
const initialState = {
    isEditing:{
        status:false,
        layer:null,
        formNuevoUsuarioIsOpen:false,
        formBookmarkIsOpen:false,

    },
    snackbar:{
      isOpen:false,
      mensaje:""
    },
    dialogContainer:{
      isOpen:false,
      component:InitialDialogContainer,
      title:"Sin titulo"
    }
}

export default function reducer(state=initialState,action){
    switch(action.type){
        case "IS_EDITING":
          state = {...state,isEditing:action.payload}
        break
        case "OPEN_FORM_NUEVOUSUARIO":
          state = {...state,formNuevoUsuarioIsOpen:!state.formNuevoUsuarioIsOpen}
        break
        case "OPEN_FORM_BOOKMARKS":
          state = {...state,formBookmarkIsOpen:!state.formBookmarkIsOpen}
        break;
        case "SNACKBAR_ON":
        const snackbar = {
          isOpen:true,
          mensaje:action.payload
        }
          state = {...state,snackbar:snackbar}
        break;
        case "SNACKBAR_OFF":
        state = {...state,snackbar:{isOpen:false,mensaje:""}}
        break
        case "DIALOGCONTAINER_OPEN":
        state = {...state,dialogContainer:action.payload}
        break
        case "DIALOGCONTAINER_CLOSE":
        state = {...state,dialogContainer:{...state.dialogContainer,isOpen:false}}
        break

    }

    return state

}
