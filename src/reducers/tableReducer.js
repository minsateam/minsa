const initialState = {
    table: {
        visible: false,
        data: [],
        layerId: "",
        columns: [],
        layerTable: null
    }
}

export default function reducer(state = initialState, action) {
    const prevTable = state.table
    let data
    switch (action.type) {
        case "OPEN_TABLE":
            data = getData(action.payload)

            state = { ...state, table: { ...prevTable, visible: true, data: data.data, columns: data.columns, layerId: data.layerid } }
            break
        case "CLOSE_TABLE":
            const layerid = action.payload
            if (layerid) {
                if (layerid == state.table.layerId) {
                    state = { ...state, table: { ...prevTable, data: [], columns: [], visible: false } }

                } else {
                    break
                }

            } else {
                state = { ...state, table: { ...prevTable, visible: false } }
            }
            break
        case "UPDATE_TABLE":

            data = getData(action.payload)
            state = { ...state, table: { ...prevTable, data: data.data, columns: data.columns, layerId: data.layerid } }
            break
        case "SET_LAYER_TABLE":
            state = { ...state, layerTable: action.payload }
            break
    }
    return state
}

function getData(layer) {
    const layerid = layer.get('id')
    const headers = layer.get("headers")
    console.log(headers)
    let columns = []
    for (const h in headers) {
        columns.push({
            header: h.toUpperCase(),
            accessor: h
        })
    }
    columns.push({
        header: "ObjectIdMap",
        accessor: "objidmap"
    })

    let data = []
    const features = layer.getSource().getFeatures()
    data = features.map((feature) => {
        const properties = feature.getProperties()
        delete properties.geometry
        Object.assign(properties, {
            objidmap: feature.getId()
        })
        return properties
    }

    )

    return {
        data: data,
        columns: columns,
        layerid: layerid
    }
}