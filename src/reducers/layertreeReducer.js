const initialState = {
    isVisible: false
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case "TOGGLE_LAYERTREE":
            state = { ...state, isVisible: !state.isVisible }
            break
    
    }
    return state
}