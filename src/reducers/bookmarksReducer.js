const initialState = {
  bookmarks:[]
}

export default function reducer(state=initialState,action){
  switch (action.type) {
    case "GET_BOOKMARKS":
      state = {...state,bookmarks:action.payload}
      break;
    case "SAVE_NEW_BOOKMARK":
      state = {...state,bookmarks:state.bookmarks.concat(action.payload)}
      break
    case "DELETE_BOOKMARK":
      const index = state.bookmarks.findIndex((bookmark) => {
        return action.payload.id == bookmark.id
      })
      state = {...state,
        bookmarks:[
          ...state.bookmarks.slice(0,index),
          ...state.bookmarks.slice(index+1,state.bookmarks.length)
        ]
      }
      break
    case "EDITAR_BOOKMARK":
    const indexOldBookmark = state.bookmarks.findIndex((bookmark) => {
      return action.payload.id == bookmark.id
    })
    state = {...state,
      bookmarks:[
        ...state.bookmarks.slice(0,indexOldBookmark),
        ...state.bookmarks.slice(indexOldBookmark+1,state.bookmarks.length),
        action.payload
      ]
    }


  }
  return state
}
