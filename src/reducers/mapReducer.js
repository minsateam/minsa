import ol from 'openlayers'
import axios from 'axios'

Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};

const initialState = {
    map: null,
    layers: [],
    capability: [],
    layerResalto: null,
    selectInteraction: null,
    spinerVisible: "hidden",
    selectedFeatures: [],
    snapI: null,
    featureResaltado:null
}
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case "SET_MAP":
            state = { ...state, map: action.payload }
            break

        case "CHANGE_LAYERS":

            const newlayers = state.map.getLayers().getArray()
                .filter((layer) => {
                    if (layer instanceof ol.layer.Group) {

                    } else {
                        return layer
                    }
                })
            state = { ...state, layers: newlayers }
            break

        case "MOVER_LAYER_ARRIBA":

            break
        case "MOVER_LAYER_ABAJO":

            break
        case "GET_CAPABILITIES":
            state = { ...state, capability: action.payload }

            break
        case "SET_LAYERRESALTO":
            state = { ...state, layerResalto: action.payload }
            break
        case "SET_SELECTINTERACTION":
            state = { ...state, selectInteraction: action.payload }
            break
        case "SPINER_ON":
            state = { ...state, spinerVisible: "visible" }
            break
        case "SPINER_OFF":
            state = { ...state, spinerVisible: "hidden" }
            break
        case "ADD_SELECTED_FEATURE":
            state = { ...state, selectedFeatures: state.selectedFeatures.concat(action.payload) }
            break
        case "REMOVE_SELECTED_FEATURE":
            const index = state.selectedFeatures.findIndex((f) => f.getId() == action.payload.getId())
            const selectedFeatures = [
                ...state.selectedFeatures.slice(0, index),
                ...state.selectedFeatures.slice(index + 1, state.selectedFeatures.length)
            ]
            state = { ...state, selectedFeatures: selectedFeatures }
            break
        case "ADD_SNAP":
            state = { ...state, snapI: action.payload }
            break
        case "SET_FEATURE_RESALTADO":
            state = { ...state, featureResaltado: action.payload }
            break



    }
    return state
}

