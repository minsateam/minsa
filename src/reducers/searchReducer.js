const initState = {
    results: [],
    endSearch: false,
    countResult:0

}
export default function reducer(state = initState, action) {
    switch (action.type) {
        case "ADD_RESULT":
            const result = {
                feature: action.payload.feature,
                value: action.payload.value,
                typeName: action.payload.typeName
            }
            const newResult = state.results.concat(result)
            const countResult = newResult.length
            state = { ...state, results:newResult ,countResult:countResult}
            break
        case "CLEAR_RESULTS":
            state = { ...state, results: [],countResult:0 }
            break
        case "END_SEARCH":
            state = { ...state, endSearch: action.payload,countResult:state.results.length }
            break
    
    }
    return state


}