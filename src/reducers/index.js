import {combineReducers} from 'redux'
import userReducer from './userReducer'
import mapReducer from './mapReducer'
import layertreeReducer from './layertreeReducer'
import tableReducer from './tableReducer'
import appReducer from './appReducer'
import layerReducer from './layerReducer'
import searchReducer from './searchReducer'
import bookmarksReducer from './bookmarksReducer'
import printReducer from './printReducer'

const reducers = combineReducers({
    user:userReducer,
    map:mapReducer,
    layertree:layertreeReducer,
    table:tableReducer,
    app:appReducer,
    layer:layerReducer,
    search:searchReducer,
    bookmarks:bookmarksReducer,
    printInfo:printReducer
})

module.exports = reducers
