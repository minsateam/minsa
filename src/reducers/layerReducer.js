const initialState = {
    isAtributeVisible: false,
    featuresinfo: {
        reciviendo: false,
        data: null,
        error: null,
        visible:false
    }
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case "TOGGLE_ATRIBUTES":
            const newstate = !state.isAtributeVisible
            state = { ...state, isAtributeVisible: newstate }
            break
        case "FEATURE_INFO_PENDING":
            state = { ...state, featuresinfo: { ...state.featuresinfo, reciviendo: true } }
            break
        case "FEATURE_INFO_FULFILLED":
            if(action.payload.data.features.length>0){
                state = {
                     ...state, 
                    featuresinfo: { ...state.featuresinfo, reciviendo: false, data: action.payload,visible:true }}
            }
            break
        case "FEATURE_INFO_REJECTED":
            state = { ...state, featuresinfo: { ...state.featuresinfo, reciviendo: false, error: action.payload,visible:false } }
            break
        case "CLOSE_FEATURE_INFO":
            state = {...state,featuresinfo:{...state.featuresinfo,visible:false}}
            break

    }
    return state
}
