const initialState = {
  printInfo:{},
  fetched:false,
  fetching:false,
  error:false
}

export default function reducer(state=initialState,action){
  switch (action.type) {
    case "INFO_PRINT_FULFILLED":
      state = {...state,printInfo:action.payload,fetched:true,fetching:false}
      break;
    case "INFO_PRINT_PENDING":
      state = {...state,fetching:true,fetched:false}
    default:
      return state
  }
  return state
}
