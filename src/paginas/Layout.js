/**
 * Created by luissolano on 01/12/17.
 */
import React from 'react';

import Footer from "../components/Footer";
import { connect } from 'react-redux'

import Map from '../components/Map'
import Botones from '../components/Botones.jsx'
import LayerTree from '../components/LayerTree.jsx'
import { css } from 'glamor'
import Tabla from '../components/Tabla.jsx'
import { Segment, Container } from 'semantic-ui-react'
import { Resizable, ResizableBox } from 'react-resizable';
import {getBoookmarks} from '../actions/bookmarkActions'
import { NotificationContainer, NotificationManager } from 'react-notifications'
import Spinner from 'react-spinkit'
import AppBar from 'material-ui/AppBar'

//Actions
import {snackBarOff,dialogContainerClose} from '../actions/appActions'
import store from '../store/store'
import { closeTable } from '../actions/tableActions'
import {closeFeatureInfoWms} from '../actions/layerActions'


import NuevoUsuario from '../components/Botones/NuevoUsuario.jsx'
import Parse from 'parse'
import AppBarLog from '../components/AppBarLog.jsx'
import LoginForm from '../components/LoginForm.jsx'
import CircularProgress from 'material-ui/CircularProgress';
import Bookmarks from '../components/Bookmarks/Bookmarks'
import Snackbar from 'material-ui/Snackbar';
import DialogContainer from './DialogContainer.jsx'
import InfoWindowFeature from '../components/InfoWindowFeature.jsx'



import injectTapEventPlugin from "react-tap-event-plugin"
injectTapEventPlugin();




@connect((store) => {
    return {
        table: store.table.table,
        map: store.map.map,
        layerResalto: store.map.layerResalto,
        spinerVisible: store.map.spinerVisible,
        featureResaltado: store.map.featureResaltado,
        formNuevoUsuarioIsOpen: store.app.formNuevoUsuarioIsOpen,
        formBookmarkIsOpen:store.app.formBookmarkIsOpen,
        currentUser:store.user.user,
        bookmarks:store.bookmarks.bookmarks,
        snackbar:store.app.snackbar,
        dialogContainer:store.app.dialogContainer,
        featuresinfo:store.layer.featuresinfo
    }
}
)
export default class Layout extends React.Component {
    constructor(props) {
        super(props)
    }
    onCerrarTabla() {
        this.props.dispatch(closeTable())
    }
    componentDidMount() {
      this.props.dispatch(getBoookmarks())
    }
    handleRequestSnackbarClose(){
      store.dispatch(snackBarOff())
    }
    onCerrarDialogContainer = ()=>{
        this.props.dispatch(dialogContainerClose())
    }

    render() {
        const Ejemplo = ()=>(
            <div>Hola Mundo</div>
        )

        const nombre = this.props.nombreUser
        const tablecontainer = css({
            position: "absolute",
            bottom: "0px",
            right: "0",
            left: "0",
            height: "300px",
            backgroundColor: "white",
            zIndex: "10",
            borderTop: "2px solid yellow",
            overflow: "scroll"
        })
        const ruleSpiner = css({
            position: "absolute",
            zIndex:1000,
            top: "45%",
            left: "45%",
            with: "400px",
            height: "400px",
            visibility: this.props.spinerVisible
        })

        const container = css({

        })

        const visible = this.props.table.visible
        const verTabla = () => {
            if (visible) {

                return <div {...tablecontainer}>
                    <Tabla
                        layerResalto={this.props.layerResalto}
                        map={this.props.map}
                        onCerrarTabla={this.onCerrarTabla.bind(this)}
                        data={this.props.table}
                        columns={this.props.table.columns}
                        featureResaltado={this.props.featureResaltado}
                    />
                </div>
            }
        }
        const renderMap = () => {
            if (this.props.currentUser) {
                return (
                    <div {...container}>
                         <Botones />
                         <Map />
                    </div>
                )
            } else {
                return <LoginForm />
            }

        }

        return (

            <div>
                <AppBarLog
                map={this.props.map}
                logged={this.props.currentUser} />

                {renderMap()}

                {verTabla()}
                <div {...ruleSpiner}>
                    <CircularProgress size={80} thickness={9} />

                </div>
                <NotificationContainer />
                <Snackbar
                    open={this.props.snackbar.isOpen}
                    message={this.props.snackbar.mensaje}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestSnackbarClose}
                />
                <NuevoUsuario
                    open={this.props.formNuevoUsuarioIsOpen} />
                <Bookmarks
                  isOpen={this.props.formBookmarkIsOpen}
                  bookmarks={this.props.bookmarks}
                  currentUser = {this.props.currentUser}
                  map = {this.props.map}
                />
                <DialogContainer
                open={this.props.dialogContainer.isOpen}
                component = {this.props.dialogContainer.component}
                title={this.props.dialogContainer.title}
                onCerrarDialog={this.onCerrarDialogContainer}
                props = {this.props.dialogContainer.propiedades}
                 />
                 {this.props.featuresinfo.data &&
                     this.props.featuresinfo.data.data.features.length>0 &&
                     this.props.featuresinfo.visible &&
                     <InfoWindowFeature map={this.props.map}
                     close={()=>this.props.dispatch(closeFeatureInfoWms())}
                      feature={this.props.featuresinfo}/>}
            </div>

        );
    }
}
