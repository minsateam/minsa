/*
    Contenedor generico para cualquier componenete que requiera ser mostrado con interface de caja de dialogo
    su estado es controlado por la accion dialogContainerOpen y dialogContainerClose en appActions
*/ 
import React from 'react';
import AppBar from 'material-ui/AppBar';
import classNames from 'classnames'
import './DialogContainer.css'
import Draggable from 'react-draggable'
import IconCerrar from 'material-ui/svg-icons/navigation/close'
import IconButton from 'material-ui/IconButton';


const DialogContainer = ({open,component,title,onCerrarDialog,props}) => {
    const dialogClass = classNames({
        'dialog-container-open':open,
        'dialog-container-close':true
    })
        return (
            
            <div className={dialogClass}>
                <AppBar
                    title={title}
                    titleStyle={{fontSize:"18px"}}
                    iconClassNameRight="muidocs-icon-navigation-expand-more"
                    iconElementRight={<IconButton><IconCerrar/></IconButton>}
                    onRightIconButtonTouchTap={onCerrarDialog}
                />
                {React.createElement(component,{...props})}
                
            </div>
           
        );
   
}

export default DialogContainer;