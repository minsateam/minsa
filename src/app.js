/**
 * Created by luissolano on 01/12/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './paginas/Layout';
import { Provider } from 'react-redux'
import store from './store/store'
import { addLocaleData, IntlProvider } from 'react-intl';
import enMessages from 'boundless-sdk/locale/en';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Parse from 'parse'
import {currentUser} from './actions/userActions'

Parse.initialize("minsapmap");
Parse.serverURL = 'http://minsa-login.herokuapp.com/parse'

const user = Parse.User.current();
if (user) {
    store.dispatch(currentUser(user))
} else {
    // show the signup or login page
}

const app = document.getElementById('app');
ReactDOM.render(
    <Provider store={store}>
        <IntlProvider locale='en' messages={enMessages}>
            <MuiThemeProvider>
                <Layout />
            </MuiThemeProvider>
        </IntlProvider>


    </Provider>
    ,
    app);