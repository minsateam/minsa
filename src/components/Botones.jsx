import React, { PropTypes } from 'react';
import { css } from 'glamor'
//actions
import * as Actions from '../actions/layertreeActions'
import * as ActionsMap from '../actions/mapActions'
import { openFormNuevoUsuario } from '../actions/appActions'

import { connect } from 'react-redux'
import store from '../store/store'
import { Button, Icon, Menu, Label } from 'semantic-ui-react'
import AddLayerLS from './Botones/AddLayerLS.jsx'
import AddLayer from 'boundless-sdk/components/AddLayer'
import AddLayerServices from './Botones/AddLayerServices.jsx'
import MenuServices from './MenuServices.jsx'
import { crearLayerImage } from '../services/geoprocesos'
import Perfil from './Botones/Perfil.jsx'
import Graficas from './Botones/Graficas.jsx'
import AtributosForm from './layer/BotonesWFS/AtributosForm.jsx'
import RaisedButton from 'material-ui/RaisedButton'

import Toc from 'material-ui/svg-icons/action/toc'
import IconButton from 'material-ui/IconButton';

import DatePicker from 'material-ui/DatePicker'


import IconMenu from 'material-ui/IconMenu';

import FontIcon from 'material-ui/FontIcon';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'
import MenuItem from 'material-ui/MenuItem'
import DropDownMenu from 'material-ui/DropDownMenu'
import Busqueda from './Busqueda/Busqueda.jsx'
import NuevoUsuario from './Botones/NuevoUsuario.jsx'
import { NotificationManager } from 'react-notifications'
import Parse from 'parse'

import LocalEdit from './Botones/LocalEdit.jsx'
import Print from './Botones/Print.jsx'

import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar'

@connect((store) => {
    return {
        map: store.map.map,
        layers: store.map.layers,
        capability: store.map.capability,
        layerResalto: store.map.layerResalto,
        selectInteraction: store.map.selectInteraction,
        selectedFeatures: store.map.selectedFeatures,
        isAtributeVisible: store.layer.isAtributeVisible,
        isEditing: store.app.isEditing,
        searchResults: store.search.results,
        countResult: store.search.countResult,
        endSearch: store.search.endSearch

    }

})
class Botones extends React.Component {
    constructor(props) {
        super(props);
    }
    openLayertree(e) {
        this.props.dispatch(Actions.toggleLayerTree())

    }
    addLayerTest() {
        this.props.dispatch(
            ActionsMap.addLayer({
                titulo: "Mi Nuevo Layer",
                id: "343343",
                visible: true
            })
        )
    }
    onClickItemMenuServices(layername,titulo) {
      
        this.props.map.addLayer(crearLayerImage(layername,titulo))

    }




    opneFormNuevoUsuario = () => store.dispatch(openFormNuevoUsuario())

    render() {
        const rule = css({
            backgroundColor: "rgba(0,0,0,0.7)"
        })
        const rulebotones = css({
            backgroundColor: "rgba(0,0,0,0.6)",
            color: "white"
        })

        return (
            <Toolbar>
                <ToolbarGroup firstChild={true}>
                    <IconButton
                        tooltipPosition="bottom-right"
                        onTouchTap={this.openLayertree.bind(this)}
                        tooltip={"Tabla de contenido"}>
                        <Toc />
                    </IconButton>

                </ToolbarGroup>
                <ToolbarGroup>

                    <MenuServices
                        capability={this.props.capability}
                        onClickItemMenuServices={this.onClickItemMenuServices.bind(this)}
                    />

                </ToolbarGroup>
                <ToolbarGroup>

                <ToolbarSeparator />

                <LocalEdit map={this.props.map}
                 spinerOn={()=>this.props.dispatch(ActionsMap.spinerOn())}
                 spinerOff={()=>this.props.dispatch(ActionsMap.spinerOff())}/>
                </ToolbarGroup>

                <ToolbarGroup>

                    <AddLayerLS />
                    <AddLayer map={this.props.map} />

                </ToolbarGroup>
                <ToolbarGroup>
                    <Graficas
                        listaLayers={this.props.layers}
                        capability={this.props.capability}
                        map={this.props.map}
                        layerResalto={this.props.layerResalto}
                        selectInteraction={this.props.selectInteraction}
                        selectedFeatures={this.props.selectedFeatures}
                    />

                    <Perfil map={this.props.map} />
                  <Print map={this.props.map}/>

                </ToolbarGroup>
                <ToolbarGroup lastChild={true}>
                    <Busqueda
                        searchResults={this.props.searchResults}
                        endSearch={this.props.endSearch}
                        countResult={this.props.countResult}
                    />
                    <FontIcon className="muidocs-icon-custom-sort" />

                    <AtributosForm
                        isAtributeVisible={this.props.isAtributeVisible}
                        features={this.props.selectedFeatures}
                        isEditing={this.props.isEditing}
                        selectInteraction={this.props.selectInteraction}
                    />

                </ToolbarGroup>
            </Toolbar>
        )

    }

}

export default Botones;
