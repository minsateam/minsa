import React from 'react';
import Parse from 'parse'
import { Checkbox, Segment, Dropdown, Popup, Grid, Header, Button, Label, Icon } from 'semantic-ui-react'
import Visible from './layer/Visible.jsx'
import { css } from 'glamor'
import { eliminarCapa, addSelectedFeature, removeSelectedFeature, setSelectInteraction } from '../actions/mapActions'
import { dialogContainerOpen } from '../actions/appActions'
import store from '../store/store'
import {
    solicitarVectoresPorDibujo, saveNewFeatureToGeoserver,
    solicitarVectoresPorFilter, getLeyendGraphic, validarUserEdit,
    saveElementToParse
} from '../services/geoprocesos'
import { openTable, closeTable, updateTable } from '../actions/tableActions'
import SelectTools from './Botones/SelectTools.jsx'
import ol from 'openlayers'
import uuidv1 from 'uuid/v1'
import BotonesWMS from './layer/BotonesWMS.jsx'
import BotonesWFS from './layer/BotonesWFS.jsx'
import { NotificationManager } from 'react-notifications'
import proj4 from 'proj4'
import QueryLayer from './layer/QueryLayer.jsx'
import axios from 'axios'
import Exportar from './layer/Exportar.jsx'

class Layer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.layer.getVisible(),
            opacityLayer: this.props.layer.getOpacity(),
            selectInteraction: null,
            dragBox: null,
            layerDibujo: null,
            statusNuevoElemento: false,
            modifyI: null,
            geomType: null,
            parameters: "",
            legendUrl: "",
            showLegend: false
        }


    }
    onMouseOver(e) {


    }
    onToggleVisible() {
        const layer = this.props.layer
        layer.setVisible(!layer.getVisible())
        this.setState({
            visible: !this.state.visible
        })


    }
    onEliminarCapa() {
        this.props.onEliminarCapa(this.props.layer)
    }
    onAcercarCapa() {
        this.props.onAcercarCapa(this.props.layer)
    }
    openTable() {
        this.props.onOpenTable(this.props.layer)
    }
    onMoverArriba() {
        this.props.onMoverLayerArriba(this.props.layer)

    }
    onMoverAbajo() {
        this.props.onMoverLayerAbajo(this.props.layer)

    }
    onSolicitarVectores() {
        this.props.onSolicitarVectores(this.props.layer)
    }
    onSolicitarVectoresPorFilter(params) {
        console.log(params)
        const {layer} = this.props
        const newlayervector = solicitarVectoresPorFilter(params.LAYERS, params.cql_filter,layer.get('titulo'))
        this.props.map.addLayer(newlayervector)
    }

    onChangeOpacity(e) {
        this.props.layer.setOpacity(e.target.value)
        this.setState({
            opacityLayer: e.target.value
        })

    }
    onDibujarPoligono() {
        const dibujarI = this.state.dibujarI

        dibujarI.setActive(true)

    }
    onSetCql_filters(parameters) {
        this.setState({
            parameters: parameters
        })
    }

    onClickItemQuery = () => {

        const layer = this.props.layer
        const onSetCql_filters = this.onSetCql_filters.bind(this)
        const props = {
            layer: layer,
            onSetCql_filters: onSetCql_filters
        }
        store.dispatch(dialogContainerOpen(QueryLayer, props, "Query Layer"))

    }
    onFinalizarDibujo(feature) {

        const dibujarI = this.state.dibujarI
        const layerName = this.props.layer.get('nombre')
        const layerDibujo = this.state.layerDibujo
        const formatWKT = new ol.format.WKT()
        proj4.defs('EPSG:32617', "+proj=utm +zone=17 +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
        const proj32617 = ol.proj.get('EPSG:32617')

        const wkts = formatWKT.writeFeature(feature, {
            dataProjection: proj32617,
            featureProjection: 'EPSG:3857'
        })
        console.log(wkts)
        dibujarI.setActive(false)
        const layer = solicitarVectoresPorDibujo(layerName, wkts)
        this.props.map.addLayer(layer)

    }

    crearDibujarInteraction() {
        const map = this.props.map
        const drawlayer = new ol.layer.Vector({
            source: new ol.source.Vector({

            }),
            nombre: "layer de dibujo",
            type: "vector",
            id: uuidv1()
        })
        const drawI = new ol.interaction.Draw({
            source: drawlayer.getSource(),
            type: 'Polygon'
        })
        drawI.on('drawend', (evt) => {
            console.log("finalizando dibujo para solicitar vectores")
            this.onFinalizarDibujo(evt.feature)
        })
        drawI.setActive(false)
        map.addInteraction(drawI)
        this.setState({
            dibujarI: drawI,
            layerDibujo: drawlayer
        })

    }
    crearSelectInteraction() {
        const layer = this.props.layer
        const map = this.props.map
        const styleConfig = {
            fill: new ol.style.Fill({
                color: 'rgba(0,255,255,0.8)'
            }),
            stroke: new ol.style.Stroke({
                color: '#00B8FF',
                width: 3
            })

        }
        const image = new ol.style.Circle({
            fill: styleConfig.fill,
            stroke: styleConfig.stroke,
            radius: 9
        })
        const seleccion = new ol.interaction.Select({
            style: [new ol.style.Style({ ...styleConfig, image: image })],
            layers: [layer]
        })
        seleccion.getFeatures().on('add', (evt) => {
            const featureAdded = evt.element
            console.log(featureAdded)
            store.dispatch(addSelectedFeature(featureAdded))

        })
        seleccion.getFeatures().on('remove', (evt) => {
            store.dispatch(removeSelectedFeature(evt.element))
        })
        seleccion.setActive(false)
        seleccion.on('change:active', (evt) => {
            if (seleccion.getActive()) {
                document.body.style.cursor = 'pointer';
            } else {
                document.body.style.cursor = 'default'
            }

        })

        map.addInteraction(seleccion)
        this.setState({
            selectInteraction: seleccion

        })
        store.dispatch(setSelectInteraction(seleccion))


        const dragBox = new ol.interaction.DragBox({
            condition: ol.events.condition.platformModifierKeyOnly
        })
        dragBox.on('boxend', function () {

            var extent = dragBox.getGeometry().getExtent();
            layer.getSource().forEachFeatureIntersectingExtent(extent, function (feature) {
                seleccion.getFeatures().push(feature);
            })

        })
        dragBox.on('boxstart', function () {
            seleccion.getFeatures().clear()
        })
        dragBox.setActive(false)
        map.addInteraction(dragBox)
        this.setState({
            dragBox: dragBox
        })
    }
    crearDrawNewElement(geomtype) {
        const map = this.props.map
        const sourceVector = this.props.layer.getSource()
        const headers = this.props.layer.get('headers')
        const nuevoInt = new ol.interaction.Draw({
            source: sourceVector,
            type: geomtype,
            snapTolerance: 1,
            geometryName: "geom"
        })
        nuevoInt.on('drawend', (evt) => {

            validarUserEdit(evt.feature, this.props.layer.get('nombreservicio'), 'newfeature')
            //saveNewFeatureToGeoserver(evt.feature, this.props.layer.get('nombreservicio'))
            store.dispatch(updateTable(this.props.layer))

        })
        nuevoInt.setActive(false)
        map.addInteraction(nuevoInt)
        this.setState({
            drawNewElement: nuevoInt
        })
    }
    crearDrawNewElementForParse(geomtype) {
        console.log(geomtype)
        const map = this.props.map
        const sourceVector = this.props.layer.getSource()
        const nuevoInt = new ol.interaction.Draw({
            source: sourceVector,
            type: geomtype,
            snapTolerance: 1
        })
        console.log(nuevoInt)
        this.props.layer.getSource().on('addfeature',this.saveToParse)
        nuevoInt.on('drawend', (evt) => {
            store.dispatch(updateTable(this.props.layer))
            //saveElementToParse(this.props.layer.get('id'), this.props.layer)
        })
        nuevoInt.setActive(false)
        map.addInteraction(nuevoInt)
        this.setState({
            drawNewElement: nuevoInt
        })
    }

    saveToParse = (evt)=>{
        saveElementToParse(this.props.layer.get('id'),this.props.layer)
    }



    crearSnapElement() {
        const map = this.props.map
        const sourceVector = this.props.layer.getSource()
        const snapI = new ol.interaction.Snap({
            source: sourceVector
        })
        snapI.setActive(false)
        map.addInteraction(snapI)
        this.setState({
            snapI: snapI
        })
        snapI.on('change:active', () => {
            console.log("activando snaping")
        })

    }
    onEstablecerMaximaResolucion() {
        const layer = this.props.layer
        layer.setMaxResolution(this.props.map.getView().getResolution())
        NotificationManager.success("Maxima resolucion establecida para esta capa", "Notificacion de cambio de resolucion")
    }
    setEventClickFeatureInfo = () => {
        const { map } = this.props
        map.on('click', this.getGetFeatureInfoUrl)
    }
    getGetFeatureInfoUrl = (evt) => {
        const { layer, map } = this.props
        if (layer.getVisible()) {
            const featureUrl = layer.getSource().getGetFeatureInfoUrl(evt.coordinate, map.getView().getResolution(), 'EPSG:3857', {
                INFO_FORMAT: 'application/json',
                srs: 'EPSG:3857'
            })

            if (featureUrl) {
                this.getFeatureInfoByUrl(featureUrl)
            }
        }



    }
    getFeatureInfoByUrl = (featureUrl) => {
        this.props.showFeatureInfoWms(featureUrl)
    }
    verLeyenda = () => {
        const { layer, map } = this.props
        const getLeyendUrl = getLeyendGraphic(layer.get('nombre'))
        this.setState({
            legendUrl: getLeyendUrl,
            showLegend: !this.state.showLegend
        })

    }



    componentDidMount() {
        this.crearSelectInteraction()
        this.crearDibujarInteraction()
        const { layer, map } = this.props

        if (this.props.layer.get('tipo') == "vector") {
            if (this.props.layer.get('origen') === "parse") {
                const geometria = this.props.layer.get('geometria')
                this.crearDrawNewElementForParse(geometria)
                this.setState({ geomType: geometria })
                this.crearSnapElement()
            } else {
                this.props.layer.getSource().once('addfeature', (evt) => {
                    this.crearDrawNewElement(evt.feature.getGeometry().getType())
                    this.setState({ geomType: evt.feature.getGeometry().getType() })
                    this.crearSnapElement()
                })
            }


        } else if (this.props.layer.get('tipo') == "imgservice") {
            this.props.layer.getSource().once('imageloadend', this.setEventClickFeatureInfo)
        }


    }
    componentWillUnmount() {
        const map = this.props.map
        this.props.layer.un('addfeature',this.saveToParse)
        map.removeInteraction(this.state.selectInteraction)
        map.removeInteraction(this.state.dragBox)
        map.removeLayer(this.state.layerDibujo)
        map.removeInteraction(this.state.dibujarI)
        map.removeInteraction(this.state.crearDrawNewElement)
        map.removeInteraction(this.state.snapI)
        map.removeInteraction(this.state.drawNewElement)
        map.un('click', this.getGetFeatureInfoUrl)
    }


    renderMenuTipo() {
        const rule = css({
            transform: "translateX(-100px)"
        })
        if (this.props.layer.get('tipo') == "vector") {
            return (
                <Dropdown text='Acciones'>
                    <Dropdown.Menu className={`${rule}`}>
                        <Dropdown.Item
                            icon="zoom"
                            text='Acercar a...'
                            onClick={this.onAcercarCapa.bind(this)} />
                        <Dropdown.Item>
                            <Header>Opacidad</Header>
                            <input
                                type="range"
                                name="opacidad"
                                min="0"
                                max="1"
                                step="0.1"
                                onChange={this.onChangeOpacity.bind(this)}
                                value={this.state.opacityLayer} />

                        </Dropdown.Item>
                        <Dropdown.Item
                            icon='arrow up'
                            text='Mover hacia arriba'
                            onClick={this.onMoverArriba.bind(this)} />
                        <Dropdown.Item
                            icon='arrow down'
                            text='Mover hacia abajo'
                            onClick={this.onMoverAbajo.bind(this)} />
                        <Dropdown.Item
                            icon="hide"
                            text="Establecer Maxima Resolucion"
                            onClick={this.onEstablecerMaximaResolucion.bind(this)}
                        />
                        <Dropdown.Item
                            icon='remove'
                            text='Eliminar capa'

                            onClick={this.onEliminarCapa.bind(this)} />
                        <Dropdown.Divider />
                        <Dropdown.Item>
                            <SelectTools
                                selectInteraction={this.state.selectInteraction}
                                dragBoxI={this.state.dragBox}
                                map={this.props.map}
                                layer={this.props.layer}
                            />
                        </Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item
                            icon="table"
                            text='Ver Tabla'
                            onClick={this.openTable.bind(this)} />

                        {this.props.layer.get('origen')==="parse" &&
                        <Dropdown.Item icon="download">
                            <Exportar
                                layer={this.props.layer}
                                map={this.props.map}
                                spinerOn={this.props.spinerOn}
                                spinerOff={this.props.spinerOff}
                                snackBarOn={this.props.snackBarOn}
                            />
                        </Dropdown.Item>}


                    </Dropdown.Menu>

                </Dropdown>

            )
        } else {
            return (
                <Dropdown text='Acciones'>
                    <Dropdown.Menu className={`${rule}`}>

                        <Dropdown.Item>
                            <Header>Opacidad</Header>
                            <input
                                type="range"
                                name="opacidad"
                                min="0"
                                max="1"
                                step="0.1"
                                onChange={this.onChangeOpacity.bind(this)}
                                value={this.state.opacityLayer} />

                        </Dropdown.Item>
                        <Dropdown.Item
                            icon='arrow up'
                            text='Mover hacia arriba'
                            onClick={this.onMoverArriba.bind(this)} />
                        <Dropdown.Item
                            icon='arrow down'
                            text='Mover hacia abajo'
                            onClick={this.onMoverAbajo.bind(this)} />
                        <Dropdown.Item
                            icon='remove'
                            text='Eliminar capa'

                            onClick={this.onEliminarCapa.bind(this)} />
                        <Dropdown.Divider />
                        <Dropdown.Item
                            icon="filter"
                            text="Query..."
                            onClick={this.onClickItemQuery}
                        />
                        <Dropdown.Item icon="download">
                            <Exportar
                                layer={this.props.layer}
                                map={this.props.map}
                                spinerOn={this.props.spinerOn}
                                spinerOff={this.props.spinerOff}
                                snackBarOn={this.props.snackBarOn}
                            />
                        </Dropdown.Item>



                    </Dropdown.Menu>
                </Dropdown>

            )
        }
    }

    renderBotonesPorTipoLayer() {
        if (this.props.layer.get('tipo') == "vector") {
            return (
                <BotonesWFS
                    layer={this.props.layer}
                    map={this.props.map}
                    drawNewElement={this.state.drawNewElement}
                    snapI={this.state.snapI}
                    modifyI={this.state.modifyI}
                    selectInteraction={this.state.selectInteraction}
                    geomType={this.state.geomType}


                />

            )
        } else {
            return (
                <BotonesWMS
                    onSolicitarVectores={this.onSolicitarVectores.bind(this)}
                    onSolicitarVectoresPorFilter={this.onSolicitarVectoresPorFilter.bind(this)}
                    cql_filter={this.state.parameters}
                    onDibujarPoligono={this.onDibujarPoligono.bind(this)}
                    onFinalizarDibujo={this.onFinalizarDibujo.bind(this)}
                    verLeyenda={this.verLeyenda.bind(this)}
                />
            )
        }

    }
    renderStatusEdit() {
        const status = this.props.layer.get('statusEdicion')
        if (status) {
            return (
                <Button circular floated="right" color="red" icon="write" />
            )

        } else {
            return null
        }


    }


    render() {
        const horizontal_segments = css({
            backgroundColor: "yellow !important",
            ' :hover': {
                fontStyle: "italic",
                fontWeight: "bold"

            }
        })

        const renderMenuTipo = this.renderMenuTipo()
        const renderBotonesPorTipoLayer = this.renderBotonesPorTipoLayer()
        const renderStatusEdit = this.renderStatusEdit()


        return (
            <div>
                <Segment.Group onMouseEnter={this.onMouseOver.bind(this)}>
                    <Segment.Group horizontal>
                        <Segment {...horizontal_segments}>
                            <Visible
                                onChange={this.onToggleVisible.bind(this)}
                                titulo={this.props.layer.get('titulo')}
                                layerId={this.props.layer.get('id')}
                                visible={this.state.visible} />
                        </Segment>
                        <Segment {...horizontal_segments}>
                            {renderMenuTipo}

                        </Segment>

                    </Segment.Group>
                    <Segment>
                        {renderBotonesPorTipoLayer}
                        {renderStatusEdit}


                    </Segment>

                    {this.state.legendUrl && this.state.showLegend && <Segment>
                        <h3>Leyenda</h3>
                        <img src={this.state.legendUrl} alt="Leyenda" />
                    </Segment>}


                </Segment.Group>

            </div>


        )
    }

}

export default Layer;
