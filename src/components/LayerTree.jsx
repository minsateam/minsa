import React from 'react'
import { css } from 'glamor'
import Layer from './Layer.jsx'
import ol from 'openlayers'

import store from '../store/store'
import { Segment, Header } from 'semantic-ui-react'
import _ from 'lodash'


class LayerTree extends React.Component {
    constructor(props) {
        super(props);
    }

    onEliminarCapa(layer) {
        const map = this.props.map
        map.removeLayer(layer)
    }
    onAcercarCapa(layer) {

        this.props.map.getView().fit(layer.getSource().getExtent(), {
            duration:"500"
        })
    }
    onOpenTable(layer) {
        this.props.onOpenTable(layer)
    }
    onMoverLayerArriba(layer) {
        this.props.onMoverLayerArriba(layer)

    }
    onMoverLayerAbajo(layer) {
        this.props.onMoverLayerAbajo(layer)

    }
    onSolicitarVectores(layer){
        this.props.onSolicitarVectores(layer)
    }
    renderLayers() {
        const layers = this.props.layers.slice(0)
        return _.reverse(layers).map((layer) =>
            <Layer
                key={layer.get('id')}
                layer={layer}
                map={this.props.map}
                selectInteraction = {this.props.selectInteraction}
                showFeatureInfoWms={this.props.showFeatureInfoWms}
                onEliminarCapa={this.onEliminarCapa.bind(this)}
                onAcercarCapa={this.onAcercarCapa.bind(this)}
                onOpenTable={this.onOpenTable.bind(this)}
                onMoverLayerArriba={this.onMoverLayerArriba.bind(this)}
                onMoverLayerAbajo={this.onMoverLayerAbajo.bind(this)}
                onSolicitarVectores={this.onSolicitarVectores.bind(this)}
                spinerOn={this.props.spinerOn}
                spinerOff={this.props.spinerOff}
                snackBarOn={this.props.snackBarOn}
            />)
    }

    render() {
        const renderLayers = this.renderLayers()
        const rule = css({
            position: "absolute",
            //top: 0,
            left: 0,
            width: "400px",
            height: "100%",
            zIndex: 2,
            overflowY:"scroll",
            overflowX:"visible",
            backgroundColor: "rgba(0,0,0,0.5)",
            transition: "width 0.3s"
        })
        const rulehide = css(
            rule,
            { width: 0}
        )
        const {isOpen} = this.props
        let rulestate;
        if (isOpen) {
            rulestate = rule
        } else {
            rulestate = rulehide
        }
        return (
            <div {...rulestate}>
                <Segment>
                    <Header as='h3'>Tabla de Contenido</Header>

                </Segment>

                {renderLayers}
            </div>

        )
    }

}

export default LayerTree;
