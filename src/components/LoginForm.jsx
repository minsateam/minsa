import React from 'react'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import { orange500, blue500 } from 'material-ui/styles/colors'
import { css } from 'glamor'
import Parse from 'parse'
import store from '../store/store'
import { currentUser } from '../actions/userActions'
import { spinerOn, spinerOff } from '../actions/mapActions'
import {NotificationManager} from 'react-notifications'

const style = {
    margin: 0,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
};
const styles = {
    errorStyle: {
        color: orange500,
    },
    underlineStyle: {
        borderColor: orange500,
    },
    floatingLabelStyle: {
        color: orange500,
    },
    floatingLabelFocusStyle: {
        color: blue500,
    },
};
const rules = css({
    padding: "50px"
})
const submitForm = (e) => {
    e.preventDefault()
    store.dispatch(spinerOn())
    const username = e.target.username.value
    const password = e.target.password.value
    console.log(username,password)
    Parse.User.logIn(username, password, {
        success: function (user) {
            store.dispatch(spinerOff())
            store.dispatch(currentUser(user))
            NotificationManager.info('Sesión Iniciada, Bienvenido ' + user.get('primer_nombre'))
        },
        error: function (user, error) {
            store.dispatch(spinerOff())
            NotificationManager.error('Falló el inicio de sesión '+error.message)
            
        }
    });
}

const TextFieldExampleCustomize = () => (
    <div {...rules}>
        <form onSubmit={submitForm} novalidate>
            <TextField
                floatingLabelText="Nombre de Usuario"
                floatingLabelStyle={styles.floatingLabelStyle}
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                type="text"
                name="username"

            />
            <br />
            <TextField
                floatingLabelText="Contraseña"
                //floatingLabelStyle={styles.floatingLabelStyle}
                //floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                type="password"
                name="password"
            />
            <br />
            <RaisedButton type="submit" label="Entrar" primary={true} />
        </form>

    </div>
)
const LoginForm = () => (
    <div>

        <Paper style={style} zDepth={5}>
            <TextFieldExampleCustomize />
        </Paper>
    </div>
);

export default LoginForm;