import React from 'react';
import ReactTable from 'react-table'
import '../../node_modules/react-table/react-table.css'
import '../../node_modules/react-resizable/css/styles.css'
import { Header, Icon, Button, Container, Segment } from 'semantic-ui-react'
import ol from 'openlayers'
import { Resizable, ResizableBox } from 'react-resizable';
import store from '../store/store'



class Tabla extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rowSelected: null
        }
        this.setSelectedRow = this.setSelectedRow.bind(this)

    }
    onCerrarTabla() {
        this.props.onCerrarTabla()

    }
    setSelectedRow(state, rowInfo, column) {
        if (this.props.featureResaltado) {
            const selected = this.props.featureResaltado.getId() == rowInfo.rowValues.objidmap
            return {
                style: {
                    background: selected ? "red" : null
                }
            }

        }else{
            return {
                style:{
                    background:"white"
                }
            }
        }

    }

    onClickData(state, rowInfo, column, instance) {
        const map = this.props.map
        const layerid = this.props.data.layerId
        let currentLayer
        map.getLayers().getArray().forEach((layer) => {
            if (layer.get('id') == layerid) {
                currentLayer = layer
            }
        })

        const source = currentLayer.getSource()

        return {
            onClick: e => {
                console.log(rowInfo.rowValues.objidmap)
                this.setState({
                    rowSelected: rowInfo.rowValues.objidmap
                })

                const featureOverlay = this.props.layerResalto
                featureOverlay.getSource().clear()
                const feature = source.getFeatureById(rowInfo.rowValues.objidmap)

                let highlight
                if (feature !== highlight) {
                    if (highlight) {
                        featureOverlay.getSource().removeFeature(highlight);
                    }
                    if (feature) {
                        featureOverlay.getSource().addFeature(feature);

                    }
                    highlight = feature;
                }
                const geom = feature.getGeometry()
                let position
                switch (geom.getType()) {
                    case 'MultiPolygon':
                        var poly = geom.getPolygons().reduce(function (left, right) {
                            return left.getArea() > right.getArea() ? left : right;
                        });
                        position = poly.getInteriorPoint().getCoordinates();
                        break;
                    case 'Polygon':
                        position = geom.getInteriorPoint().getCoordinates();
                        break;
                    case 'LineString':
                        position = geom.getCoordinateAt(0.5)
                        break
                    case 'MultiLineString':
                        position = geom.getFirstCoordinate()
                        break
                    case 'Point':
                        position = geom.getCoordinates()
                }
                map.getView().animate({
                    center:position
                })

            }
        }
    }

    render() {
        console.log("rendering")
        return (


            <div>
                <Segment clearing>
                    <Button icon="remove" onClick={this.onCerrarTabla.bind(this)} floated="right" />
                    <Header icon="table" floated="left" content="Tabla de Atributos"></Header>

                </Segment>

                <ReactTable
                    data={this.props.data.data}
                    columns={this.props.columns}
                    defaultPageSize={10}
                    minRows={this.props.data.data.length}
                    freezeWhenExpanded={true}
                    getTdProps={this.onClickData.bind(this)}
                    getTrProps={this.setSelectedRow}

                />
            </div>



        )
    }


}

export default Tabla;
