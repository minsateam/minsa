import React from 'react';
import { css } from 'glamor'
import Draggable from 'react-draggable'
import { Card, Icon, Button, Image, Header } from 'semantic-ui-react'
import LineImage from '../../img/Lineicon.png'
import ol from 'openlayers'
import uuidv1 from 'uuid/v1'
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts'
import { explode } from '@turf/turf'
import FlatButton from 'material-ui/FlatButton'

class Perfil extends React.Component {
    constructor() {
        super();
        this.state = {
            isVisible: "hidden",
            dibujoActive: false,
            interaction: null,
            sourceElevation: null,
            layerPerfil: null,
            layerResalto: null
        };
    }
    setVisible() {
        this.setState({
            isVisible: "visible"
        })
    }
    closeDialog() {
        const layerPerfilSource = this.state.layerPerfil
        const layerResalto = this.state.layerResalto
        if (layerPerfilSource) {
            layerPerfilSource.getSource().clear()
            layerResalto.getSource().clear()

        }
        this.setState({
            isVisible: "hidden"
        })
    }
    onStartLine() {
        this.setState({
            dibujoActive: !this.state.dibujoActive
        })
        if (this.state.interaction) {
            this.state.interaction.setActive(!this.state.interaction.getActive())
        } else {
            const map = this.props.map
            const source = new ol.source.Vector({})
            const layer = new ol.layer.Vector({
                source: source,
                map: map,
                title: "Linea del perfil",
                style: [new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: "#FD06C6",
                        width: 3
                    }),
                    image: new ol.style.Circle({
                        fill: new ol.style.Fill({
                            color: "rgba(0,0,255,0.1)"
                        }),
                        stroke: new ol.style.Stroke({
                            color: "blue",
                            width: 1
                        }),
                        radius: 5

                    })
                })]
            })
            layer.set('nombre', "linea del perfil")
            this.setState({
                layerPerfil: layer
            })
            const dibujar = new ol.interaction.Draw({
                source: source,
                type: "LineString",
                style: [new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: "#FD06C6",
                        width: 4
                    })
                })]
            })
            const sourceElevation = new ol.source.XYZ({
                url: "https://api.mapbox.com/v4/mapbox.terrain-rgb/{z}/{x}/{y}.pngraw?access_token=pk.eyJ1IjoiZ2xpYmVydGUiLCJhIjoiY2l5OTFjMGVtMDA3aDMycW91eDZ2bnBkMyJ9.0ribIg_tlpagcxJmFyFM9A",
                crossOrigin: 'anonymous'
            })
            this.setState({
                sourceElevation: sourceElevation
            })

            const layerElevation2 = new ol.layer.Tile({
                source: sourceElevation,
                map: map,
                nombre: "elevation",
                title: "Elevacion",
                id: uuidv1(),
                opacity: 0.1

            })

            dibujar.on('drawstart', (evt) => {
                source.clear()
            })
            dibujar.on('drawend', (evt) => {

                const feature = evt.feature
                let decicoords = []
                const geomline = feature.getGeometry()
                const firstcoord = geomline.getFirstCoordinate()
                decicoords.push(firstcoord)
                const longitud = geomline.getLength()
                let decimo = 0.1
                while (decimo <= 1) {
                    const newcoords = geomline.getCoordinateAt(decimo)
                    decicoords.push(newcoords)


                    decimo += 0.1
                }

                const decicoordIdFeature = []

                decicoords.forEach((coord) => {
                    const newPuntoFeature = new ol.Feature({
                        geometry: new ol.geom.Point(coord)
                    })

                    const idfeature = uuidv1()
                    newPuntoFeature.setId(idfeature)
                    decicoordIdFeature.push({
                        idcoord: idfeature,
                        coord: coord
                    })
                    source.addFeature(newPuntoFeature)
                })
                this.crearLayerResalto()

                this.calcularDistanciasYElevaciones(decicoordIdFeature)


            })
            this.setState({
                interaction: dibujar
            })
            map.addInteraction(dibujar)
        }
    }
    unirAlturasDistancias(distancias, elevaciones, coords) {
        let alturasdistancias = []
        for (let i = 0; i < distancias.length; i++) {
            const itemdistalt = {
                distancia: parseFloat(distancias[i].toFixed(2)),
                elevacion: elevaciones[i],
                idcoord: coords[i].idcoord
            }
            alturasdistancias.push(itemdistalt)
        }
        this.setState({
            alturadistancias: alturasdistancias
        })

    }
    calcularDistanciasYElevaciones(coords) {
        let distancias = [0]
        const sourceElevation = this.state.sourceElevation
        let distanciaAcumulada = 0
        let elevaciones = []
        for (let i = 0; i < coords.length - 1; i++) {
            const distanciai = Math.sqrt(Math.pow(coords[i + 1].coord[1] - coords[i].coord[1], 2) + Math.pow(coords[i + 1].coord[0] - coords[i].coord[0], 2))
            distanciaAcumulada += distanciai
            distancias.push(distanciaAcumulada)
        }
        const map = this.props.map
        const pixels = coords.map((coord) => map.getPixelFromCoordinate(coord.coord))
        pixels.forEach((pixel) =>
            map.forEachLayerAtPixel(pixel, function (layer, rgba) {
                let height = -10000 + ((rgba[0] * 256 * 256 + rgba[1] * 256 + rgba[2]) * 0.1)
                elevaciones.push(height)

            }, undefined, function (layer) {
                return layer.getSource() == sourceElevation;
            })
        )

        this.unirAlturasDistancias(distancias, elevaciones, coords)
    }
    crearLayerResalto() {
        const map = this.props.map
        const styleResaltado = new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: "(255,255,0,0.5)"
                }),
                stroke: new ol.style.Stroke({
                    color: "#FFFF00",
                    width: 2
                }),
                radius: 9
            })
        })

        const featureOverlay = new ol.layer.Vector({
            source: new ol.source.Vector(),
            map: map,
            style: [
                styleResaltado
            ]
        });
        featureOverlay.set('nombre', 'linea del perfil')

        this.setState({
            layerResalto: featureOverlay
        })
    }
    onMouseMove(data, index) {
        const map = this.props.map
        const layerResalto = this.state.layerResalto
        const layerSource = this.state.layerPerfil.getSource()
        layerResalto.getSource().clear()
        if (data.isTooltipActive) {
            const idFeature = data.activePayload[0].payload.idcoord
            const feature = layerSource.getFeatureById(idFeature)


            let highlight;


            if (feature !== highlight) {
                if (highlight) {
                    layerResalto.getSource().removeFeature(highlight);
                }
                if (feature) {
                    layerResalto.getSource().addFeature(feature);
                }
                highlight = feature;
            }



        }

    }

    render() {
        const rule = css({
            position: "absolute",
            top: "130%",
            left: "-105%",
            transform: "translate(-50%, -50%)",
            backgroundColor: "white",
            zIndex: 10,
            visibility: this.state.isVisible
        })
        const ruleGraph = css({
            position: "absolute",
            top: "130%",
            left: "40%",
            backgroundColor: "white",
            minWidth: "300px",
            zIndex: 10,
            visibility: this.state.isVisible,
            padding: "20px"
        })
        return (
            <div>
                <FlatButton
                    label="Perfil"
                    primary={true}
                    icon={<i class="fa fa-area-chart"
                        aria-hidden="true"></i>}
                    onClick={this.setVisible.bind(this)} />
                <Draggable>

                    <div {...rule}>
                        <Card>

                            <Card.Content>
                                <Card.Header>
                                    Pefil de Elevacion
                                <Button onClick={this.closeDialog.bind(this)} size="mini" floated="right" icon="remove" />
                                </Card.Header>
                                <Button inverted color="green" size="mini" active={this.state.dibujoActive} onClick={this.onStartLine.bind(this)}>
                                    <Image src={LineImage} size='mini' />
                                </Button>

                            </Card.Content>
                            <Card.Content extra>

                            </Card.Content>
                        </Card>

                    </div>
                </Draggable>
                <Draggable>
                    <div {...ruleGraph}>
                        <Header as='h3'>
                            <Icon name='area chart' />
                            <Header.Content>
                                Grafica de Perfil de Elevacion (metros vs metros)
                            </Header.Content>
                        </Header>

                        <AreaChart onMouseMove={this.onMouseMove.bind(this)} width={600} height={400} data={this.state.alturadistancias}
                            margin={{ top: 30, right: 60, left: 0, bottom: 0 }}>
                            <XAxis
                                dataKey="distancia"
                                type="number"
                                allowDecimals={true}
                                unit="metros"
                                name="distancia"
                                label="Dist.(metros)" />
                            <YAxis

                                dataKey="elevacion"
                                label="Altura(metros)"
                                unit="metros"
                                name="elevación"
                                type="number" />
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip />
                            <Area
                                type='monotone'
                                dataKey='elevacion'
                                stroke='#8884d8'
                                fill='#8884d8'
                                unit=" metros"
                            />
                        </AreaChart>
                    </div>
                </Draggable>
            </div>

        )
    }
    componentDidMount() {
        this.setState({ someKey: 'otherValue' });
    }
}
export default Perfil;
