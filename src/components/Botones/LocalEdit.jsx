import React from 'react';
import FlatButton from 'material-ui/FlatButton'
import IconCreate from 'material-ui/svg-icons/content/create'
import Parse from 'parse'
import swal from 'sweetalert'
import { cargarParseLayer } from '../../services/geoprocesos'
//actions


const LocalEdit = ({ map,spinerOn }) => {
    const user = Parse.User.current()
    const Capas = Parse.Object.extend('Capas')
    const crearNuevasCapas = (elem) => {
        const newlayer = {
            type: "FeatureCollection",
            features: [{
                type: "Feature",
                geometry: {
                    type: elem.geom,
                    coordinates: elem.coords
                },
                properties: {
                    titulo: "demo",
                    descripcion: "demo"
                }

            }]
        }
        const pointLayer = new Capas()
        pointLayer.save({
            user: user,
            tipo: elem.geom,
            data: newlayer,
            headers: {
                titulo: "string",
                descripcion: "string"
            }
        }, {
                success: (result) => {
                    const layer = cargarParseLayer(result)
                    map.addLayer(layer)
                    
                },
                error: (err) => {
                    spinerOff()
                    console.log(err)
                }
            })


    }
    const cargarCapasExistentes = (result) => {
        const layer = cargarParseLayer(result)
        map.addLayer(layer)
        
        
    }

    const onClick = () => {
        
        const elementosIniciales = [
            {
                geom: "Point",
                coords: [-79.504571, 9.034881]
            },
            {
                geom: "Polygon",
                coords: [
                    [
                        [
                            -79.54513549804688,
                            9.117012411664737
                        ],
                        [
                            -79.54513549804688,
                            9.054633645237482
                        ],
                        [
                            -79.54513549804688,
                            9.008520639259519
                        ],
                        [
                            -79.41741943359374,
                            9.013946004773622
                        ],
                        [
                            -79.41879272460938,
                            9.123792057073985
                        ],
                        [
                            -79.54513549804688,
                            9.117012411664737
                        ]
                    ]
                ]
            },
            {
                geom: "LineString",
                coords: [
                    [
                        -79.70306396484375,
                        8.933913624704113
                    ],
                    [
                        -79.56436157226562,
                        9.018014975431448
                    ],
                    [
                        -79.486083984375,
                        9.064126766278852
                    ]
                ]
            }
        ]
        const query = new Parse.Query(Capas)
        query.equalTo('user', user)
        query.find({
            success: (results) => {
                if (results.length === 0) {
                    elementosIniciales.forEach((elemento)=>{
                        crearNuevasCapas(elemento)

                    })
                   
                    swal({
                        title: "Edicion Local",
                        text: "Se han creado tres capas en tu perfil."
                    })
                    

                } else {
                    results.forEach((result) => {
                        cargarCapasExistentes(result)
                    })
                    
                    swal({
                        title: "Edicion Local",
                        text: "Capas cargadas al mapa."
                    })
                    
                }
            },
            error: (err) => {
               
                console.log(err)
            }
        })
    }
    return (
        <div>
            <FlatButton
                onTouchTap={onClick}
                labelStyle={{ color: "#00BCD4" }}
                label="Trazos" icon={<IconCreate
                    color="#00BCD4" />} />
        </div>
    );
};

export default LocalEdit;