import React from 'react';
import { Dropdown, Menu, Icon, Divider } from 'semantic-ui-react'
import 'react-notifications/lib/notifications.css'
import { NotificationManager} from 'react-notifications'
import {css} from 'glamor'
import {setSelectInteraction} from '../../actions/mapActions'
import store from '../../store/store'

class SelectTools extends React.Component {
    constructor(props) {
        super(props);
        this.state = { interactiveState: false };
    }
    seleccionInteractiva() {
        const selectI = this.props.selectInteraction
        const dragBoxI = this.props.dragBoxI
        const map = this.props.map
        selectI.setActive(!selectI.getActive())
        dragBoxI.setActive(!dragBoxI.getActive())
        store.dispatch(setSelectInteraction(selectI))
        this.setState({
            interactiveState: selectI.getActive()
        })
        if(selectI.getActive()){
            NotificationManager.success('Para seleccion multiple, manten presionado Shift. Para seleccion por rectángulo, manten presionado'+
        ' Ctrl (Win) o Cmd (Mac) mientras arrastras', 'Seleccion Interactiva',8000);
        }else{
            NotificationManager.info('Seleccion Interactiva Desactivada')
        }
    }

    limpiarSeleccion() {
        this.props.selectInteraction.getFeatures().clear()
    }

    render() {
        const rule = css({
            transform:"translateX(-260px)"

        })
        return (
            //<Dropdown.Menu vertical>
            <Dropdown item text='Seleccionar por..'>
                <Dropdown.Menu className={`${rule}`}>
                    <Dropdown.Item
                    onClick={this.seleccionInteractiva.bind(this)}
                    active={this.state.interactiveState}
                    icon="mouse pointer"
                    text="Seleccion interactiva"
                    description="Shift,Ctrl" />

                  {/*<Dropdown.Item>
                        <Icon name="filter" size="large" />
                        Mediante consulta...
                      </Dropdown.Item>*/}
                    <Divider />
                    <Dropdown.Item onClick={this.limpiarSeleccion.bind(this)} text="Limpiar seleccion" />
                </Dropdown.Menu>
            </Dropdown>
            //</Dropdown.Menu>
        )
    }

    componentDidMount() {
        
    }
}

export default SelectTools;
