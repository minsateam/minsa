import React from 'react';
import { Button, Checkbox, Form, Input, Message, Radio, Select, TextArea } from 'semantic-ui-react'

class FormAddLayer extends React.Component {
    constructor() {
        super();
        this.state = { formData: {} };
    }
    handleSubmit(e, {formData}) {
        e.preventDefault()
        const file = this.refs.file.files[0]
        formData.file = file
        this.props.onHandleSubmit(formData)
        

    }

    render() {
        const filestipos = [
            { key: "csv", text: "CSV", value: "csv" },
            { key: "shp", text: "Shapefile(zip)", value: "shapefile" }
        ]
        return (
            <Form onSubmit={this.handleSubmit.bind(this)}>
                <Form.Input type="text" label='Asignar nombre a la capa' name='nombre' placeholder='Nombre' />
                <Form.Group widths='equal'>
                    <Form.Field>
                        <label>Archivo</label>
                        <input type='file' name="filepath"  ref="file" />
                    </Form.Field>
                    <Form.Select label='Formato' name='formato' options={filestipos} placeholder='Formato de Archivo' />
                </Form.Group>
                <Button primary type='submit'>Aceptar</Button>
            </Form>
        )
    }


}

export default FormAddLayer;
