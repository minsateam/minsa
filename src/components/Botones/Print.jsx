import React, {Component} from 'react'
import axios from 'axios'

import Dialog from 'material-ui/Dialog';
import ol from 'openlayers'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

//material-ui
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'

import IconPrint from 'material-ui/svg-icons/maps/local-printshop'
import store from '../../store/store'
import {getInfo} from '../../actions/printActions'

import {getLeyendGraphic} from '../../services/geoprocesos'

@connect((store)=>{
  return {
    printInfo:store.printInfo.printInfo,
    fetching:store.printInfo.fetching,
    fetched:store.printInfo.fetched,
    error:store.printInfo.error
  }
})
class Print extends Component{

  state = {
    open: false,
    layout:"A4 Vertical",
    escala:"",
    titulo:"Nuevo Titulo",
    usegoogle:false
  };
  getResolutionForScale(scale, units) {
    var dpi = 25.4 / 0.28;
    var mpu = ol.proj.METERS_PER_UNIT[units];
    console.log(mpu)
    var inchesPerMeter = 39.37;
    return parseFloat(scale*0.0254) / (dpi*mpu*inchesPerMeter);
  }

  handleOpen = () => {
    this.setState({open: true});
    this.props.dispatch(getInfo())
    const {map} = this.props
    const bbox = map.getView().calculateExtent()
    this.setState({
      bbox:bbox
    })
  };
  handleCancel = ()=>{
    this.setState({open:false})
  }
  handleChangeTitulo = (evt,valor)=>{
    this.setState({titulo:valor})
  }
  handleUseGoogle = (evt,usegoogle)=>{
    this.setState({usegoogle})
  }
  getLayerBaseVisible = ()=>{
    const {map} = this.props
    const layerGroup = map.getLayers().getArray().filter(layer=>{
      return layer.get('title')==="Mapas Base"
    })
    const layerBaseVisible = layerGroup[0].getLayers().getArray().filter(layer=>{
      return layer.getVisible()
    })
    return layerBaseVisible[0]
  }
  getLayerWMSVisibles = ()=>{
    const {map} = this.props
    const wmsLayersVisibles = map.getLayers().getArray().filter(layer=>{
      return layer.get('tipo')==="imgservice" && layer.getVisible()
    })
    return wmsLayersVisibles
  }
  getLayerVectorVisible = ()=>{
    const {map} = this.props
    const layerVectorVisible = map.getLayers().getArray().filter(layer=>{
      return layer.get('tipo')==="vector" && layer.getVisible()
    })
    return layerVectorVisible
  }


  encodeLayer = (layer)=>{
    let enc


    if(layer.get('tipo')==="imgservice"){
      const url = layer.getSource().getUrl()
      const params = layer.getSource().getParams()

      enc = {
        baseURL:url,
        opacity:1,
        customParams:params,
        layers:[layer.get('nombre')],
        type:"WMS",
        format:'image/png'
      }

      return enc
    }else if(layer.get('tipo')==="vector"){
        let externalGraphic = null,fillColor,strokeColor,strokeWidth,pointRadius
        let styles={}

        const opacity = layer.getOpacity()
        const formatJson = new ol.format.GeoJSON()
        const features = layer.getSource().getFeatures()
        const geomType = features[0].getGeometry().getType()
        if(layer.getStyle() instanceof ol.style.Style){
          console.log(layer.getStyle())
          const style = layer.getStyle()
          const image = style.getImage()

            if(image instanceof ol.style.Icon){
              console.log("icon")
              externalGraphic = image.getSrc()
              features.forEach((feature)=>{
                feature.set('simbol','external')

              })
              styles['external']={
                externalGraphic:externalGraphic
              }
            }else{
              console.log("RegurlarShape")
              fillColor = style.getFill().getColor()
              strokeColor = style.getStroke().getColor()
              strokeWidth = style.getStroke().getWidth()
              pointRadius = style.getImage().getRadius()
              features.forEach((feature)=>{
                feature.set('simbol','geom')
              })
              styles['geom']={
                fill:true,
                fillColor:rgb2hex(fillColor),
                strokeColor:rgb2hex(strokeColor),
                pointRadius:pointRadius,
                fillOpacity:opacity
              }
            }
        }else{
          features.forEach((feature)=>{
            const styleFeature = feature.getStyle()
            console.log(styleFeature)
            //const featureFillColor = styleFeature.getFill().getColor()
            //featureStrokeColor = styleFeature.getStroke().getColor()
            //featureStrokeWidth = styleFeature.getStroke().getWidth()
            //featurePointRadius = styleFeature.getImage().getRadius()
            const featureText = feature.get(feature.get('label'))
            feature.set('simbol',{
              fontFamily:feature.get('fontFamily'),
              fontSize:feature.get('fontSize'),
              labelXOffset:feature.get('labelXOffset'),
              labelYOffset:feature.get('labelYOffset'),
              fontColor:feature.get('fontColor'),
              //labelAlign:'rt',
              label:featureText,
              fillColor:rgb2hex(feature.get('fillColor')),
              strokeColor:rgb2hex(feature.get('strokeColor')),
              strokeWidth:feature.get('strokeWidth'),
              pointRadius:feature.get('pointRadius'),
              fillOpacity:opacity

            })
            console.log(feature.get('simbol'))

          })
        }

        function rgb2hex(rgb){

         if(rgb){
           rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
           return (rgb && rgb.length === 4) ? "#" +
            ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
            ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
            ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
         }

        }

        console.log(styles)
        const featuresJson = formatJson.writeFeaturesObject(features)

        enc = {
          type:"vector",
          geoJson:featuresJson,
          styleProperty:'simbol',
          name:layer.get('nombre'),
          styles:styles
        }
        return enc
    }else{
      enc = {
        type:"OSM",
        baseURL:"http://tile.openstreetmap.org",
        extension:"png",
        singleTile:false,
        maxExtent:[-20037508.34, -20037508.34, 20037508.34, 20037508.34],
        tileSize:[256, 256],
        resolutions: [156543.03390625,78271.516953125,39135.7584765625,19567.87923828125,
          9783.939619140625,4891.9698095703125,2445.9849047851562,
          1222.9924523925781,611.4962261962891,305.74811309814453,152.87405654907226,
         76.43702827453613,38.218514137268066,19.109257068634033,9.554628534317017,
         4.777314267158508,2.388657133579254,1.194328566789627,0.5971642833948135]
      }
      return enc
    }

  }
  encodedLayers = ()=>{
      let encLyr=[]
      const {map} = this.props
      const layerBaseVisible = this.getLayerBaseVisible()
      const layerVectorVisible = this.getLayerVectorVisible()
      const layerWMSVisibles = this.getLayerWMSVisibles()

      const encodLayer = this.encodeLayer(layerBaseVisible)
      encLyr.push(encodLayer)
      layerWMSVisibles.forEach(layer=>{
          const encodeWmsLayer = this.encodeLayer(layer)
          encLyr.push(encodeWmsLayer)
      })
      layerVectorVisible.forEach(layer=>{
          const encodeLayer = this.encodeLayer(layer)
          encLyr.push(encodeLayer)
      })
      console.log(encLyr)
      return encLyr
  }

  encodeLengend = (layer)=>{
    const encLegend = {
      classes:[
        {
          icons: [getLeyendGraphic(layer.get('nombre'))],
          name:""
        }
      ],
      name:layer.get('titulo')
    }
    return encLegend
  }

  encodedLegends = ()=>{
    const encLegends = []
    const layerWMSVisibles = this.getLayerWMSVisibles()
    layerWMSVisibles.forEach(layer=>{
      if(layer.get('tipo')==="imgservice"){
        encLegends.push(this.encodeLengend(layer))
      }

    })

    return encLegends
  }
  setSpecObject = ()=>{

    this.setState({
      progress:true
    })
    const {bbox} = this.state
    const userName = "test"
    const layers = this.encodedLayers()
    const legends = this.encodedLegends()
    console.log(legends)
    const {map} = this.props
    const {escala} = this.state
    const printJson = {
      layout:this.state.layout,
      srs:"EPSG:3857",
      mapTitle:this.state.titulo,
      units:"m",
      userName:userName,
      geodetic:false,
      outputFormat:"pdf",
      outputFilename:"MapaPMAP",
      layers:layers,
      pages:[{
        bbox:bbox,
        dpi:300,
        geodetic:false,
        mapTitle:"Titulo de prueba",
        comment:"Comentarios de prueba",
        mapCopyright:"Propiedad del Minsa"
      }],
      legends:legends,
      createURL:this.props.printInfo.data.createURL
    }
    this.print(printJson)

  }
  print = (specs)=>{
    console.log(specs)
    const url = this.props.printInfo.data.createURL
    console.log(url)
    const _this = this
    axios.post(url,specs)
    .then(response=>{
      console.log(response.data)
      window.open(response.data.getURL,'_blank')
      _this.setState({
        open: false,
        progress:false
      });

    })
    .catch(err=>{
      console.log(err)
      _this.setState({
        open: false,
        progress:false
      });

    })

  }

  handleClose = () => {
    this.setSpecObject()

  };
  handleChangeLayout = (e,k,valor)=>{
    this.setState({layout:valor})
  }
  handleChangeEscala = (e,k,valor)=>{
    console.log(this.props.map.getView().getResolution())
    const res = this.getResolutionForScale(valor,'m')
    console.log(res)
    this.props.map.getView().setResolution(res*1000)
    this.setState({escala:valor})
  }
  componentDidMount() {

  }
  render(){
    const actions = [
      <FlatButton
        label="Cancelar"
        primary={true}
        onTouchTap={this.handleCancel}
      />,
      <FlatButton
        label="Crear PDF"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleClose}
      />,
    ];
    return (
      <div>
        <FlatButton icon={<IconPrint/>} label="Impr." primary={true} onTouchTap={this.handleOpen} />
        <Dialog
          title="Establecer Opciones del Layout"
          titleStyle={{textAlign:"center"}}
          actions={actions}
          modal={false}
          open={this.state.open}
          contentStyle={{width:"300px"}}
          onRequestClose={this.handleClose}
        >

          {this.props.fetching &&
            <div style={{textAlign:"center"}}>
            <CircularProgress  size={80} thickness={5}/>
            </div>}
          {this.props.fetched &&
          <div>
            <form>

              {this.state.progress &&
                <div style={{textAlign:"center"}}>
                  <CircularProgress  size={80} thickness={5}/>
                </div>
                }
              <TextField
              floatingLabelText="Titulo del mapa"
              onChange={this.handleChangeTitulo}
              value={this.state.titulo}
              /><br/>
              <SelectField
                floatingLabelText="Elegir Orientación"
                value={this.state.layout}
                onChange={this.handleChangeLayout}>
                {this.props.printInfo.data.layouts.map(layout=>{
                  return <MenuItem key={layout.name} value={layout.name} primaryText={layout.name}/>
                })}

              </SelectField>


            </form>
          </div>}


        </Dialog>
      </div>
    )
  }
}


export default Print
