import React from 'react';
import RaisedButton from 'material-ui/RaisedButton'
import { Form, Checkbox, Message } from 'semantic-ui-react'
import {NotificationManager} from 'react-notifications'
//action
import store from '../../../store/store'
import { openFormNuevoUsuario } from '../../../actions/appActions'
import Parse from 'parse'
import './FormNuevoUsuario.css'

class FormNuevoUsuario extends React.Component {
    constructor() {
        super();
        this.state = {
            passwordError: false,
            loading:false,
            error:{
                email:false,
                primerNombre:false,
                primerApellido:false,
                username:false,
                geoserver_user:false
            },
            disabled:true
         };
    }
    onChange = (e)=>{
        let result
        const emailpattern = /^[a-z][a-z0-9_]+@[a-z0-9]+[.][a-z]{2,3}$/i
        const nombrepattern = /^[a-zA-Z]+$/
        const usernamepattern = /^[a-zA-Z]+[a-zA-Z0-9_]+$/
        switch (e.target.name) {
            case "email":
                result= emailpattern.exec(e.target.value)
                if(result){
                    this.setState({
                        error:{...this.state.error,email:false},
                        disabled:false
                    })
                }else{
                    this.setState({
                        error:{...this.state.error,email:true},
                        disabled:true
                    })
                }
                break;
            case "primerNombre":
            result = nombrepattern.exec(e.target.value)
            if(result){
                this.setState({
                    error:{...this.state.error,primerNombre:false}
                })
            }else{
                this.setState({
                    error:{...this.state.error,primerNombre:true}
                })
            }
            break
            case "primerApellido":
            result = nombrepattern.exec(e.target.value)
            if(result){
                this.setState({
                    error:{...this.state.error,primerApellido:false}
                })
            }else{
                this.setState({
                    error:{...this.state.error,primerApellido:true}
                })
            }
            break
            case "username":
            result = usernamepattern.exec(e.target.value)
            if(result){
                this.setState({
                    error:{...this.state.error,username:false}
                })
            }else{
                this.setState({
                    error:{...this.state.error,username:true}
                })
            }
            break
            case "geoserver_user":
            result = usernamepattern.exec(e.target.value)
            if(result){
                this.setState({
                    error:{...this.state.error,geoserver_user:false}
                })
            }else{
                this.setState({
                    error:{...this.state.error,geoserver_user:true}
                })
            }
            break

            default:
                break;
        }

    }
    onSubmitForm = (e, d) => {
        e.preventDefault()
        /*
        const currenUser = Parse.User.current()
        console.log(currenUser)
        if(currenUser){
            Parse.User.logOut()

        }
        */
        const emailpattern = /^[a-z][a-z0-9_]+@[a-z0-9]+[.][a-z]{2,3}$/i


        if (d.formData.password1 != d.formData.password2) {
            this.setState({
                passwordError: true
            })
        }else{
            this.setState({
                passwordError:false,
                loading:true
            })

            const user = new Parse.User()
            user.set('username',d.formData.username)
            user.set('password',d.formData.password1)

            user.set('email',d.formData.email)


            user.set('primer_nombre',d.formData.primerNombre)
            user.set('primer_apellido',d.formData.primerApellido)
            user.set('esAdmin',d.formData.esAdmin)
            user.set('geoserver_user',d.formData.geoserver_user)
            const _this = this
            user.save(null,{
                success(user){
                    _this.setState({
                        loading:false
                    })
                    store.dispatch(openFormNuevoUsuario())
                    NotificationManager.success('Usuario Creado exitosamente')

                },
                error(err){
                    _this.setState({
                        loading:false
                    })
                    NotificationManager.error('No se pudo crear el usuario '+err.message)

                }
            })

        }
    }
    onCancelarForm = () => store.dispatch(openFormNuevoUsuario())

    render() {
        return (
            <Form loading={this.state.loading}   onSubmit={this.onSubmitForm}>
                <Form.Field error={this.state.error.primerNombre}>
                    <label>Primer Nombre</label>
                    <input onChange={this.onChange} placeholder='Primer Nombre' name="primerNombre" required />
                </Form.Field>
                {this.state.error.primerNombre && <span class="theerror">Caracteres no aceptables.</span>}
                <Form.Field error={this.state.error.primerApellido}>
                    <label>Primer Apellido</label>
                    <input onChange={this.onChange} placeholder='Primer Apellido' name="primerApellido" required />
                </Form.Field >
                {this.state.error.primerApellido && <span class="theerror">Caracteres no aceptables.</span>}
                <Form.Field error={this.state.error.username}>
                    <label>Nombre de Usuario</label>
                    <input onChange={this.onChange}  placeholder='Nombre de Usuario' name="username" required />
                </Form.Field>
                {this.state.error.username && <span class="theerror">Caracteres no aceptables.</span>}
                <Form.Field error={this.state.error.email} >
                    <label>Email</label>
                    <input

                     placeholder='email' onChange={this.onChange} type="email" name="email" required />

                </Form.Field>
                {this.state.error.email && <span class="theerror">Email no válido</span>}
                <Form.Field error={this.state.error.geoserver_user} >
                    <label>Geoserver User</label>
                    <input

                     placeholder='geoserver user' onChange={this.onChange} type="text" name="geoserver_user" required />

                </Form.Field>
                {this.state.error.geoserver_user && <span class="theerror">Caracteres no validos</span>}
                <Form.Field error={this.state.passwordError} >
                    <label>Contraseña</label>
                    <input placeholder='contraseña' type="password" name="password1" required />

                </Form.Field>
                {this.state.passwordError && <span class="theerror">Contraseñas no coinciden.</span>}

                <Form.Field>
                    <label>Repetir Contraseña</label>
                    <input placeholder='contraseña' type="password" name="password2" required />
                </Form.Field>
                <Form.Field>
                    <Checkbox label='Usuario Administrativo' name="esAdmin" />
                </Form.Field>
                <RaisedButton type='submit' label="Aceptar" disabled={this.state.disabled} primary={true} />
                <RaisedButton type='button' label="Cancelar" secondary={true} onTouchTap={this.onCancelarForm} />
            </Form>

        )
    }

    componentDidMount() {
        this.setState({ someKey: 'otherValue' });
    }
}

export default FormNuevoUsuario;
