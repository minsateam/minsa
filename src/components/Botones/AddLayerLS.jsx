import React from 'react';
import { Button, Header, Modal, Icon } from 'semantic-ui-react'
import FormAddLayer from './AddLayer/FormAddLayer.jsx'
import ol from 'openlayers'
import { connect } from 'react-redux'
import store from '../../store/store'
import Papa from 'papaparse'
import GeoJSON from 'geojson';
import alertify from 'alertifyjs'
import '../../../node_modules/alertifyjs/build/css/alertify.css'
import { crearVectorLayer } from '../../services/geoprocesos'
import shp from 'shp2json-js'
import RaisedButton from 'material-ui/RaisedButton'


@connect((store) => {
    return {
        map: store.map.map
    }
})
class AddLayerLS extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalOpen: false,
            formData: {}
        }

    }
    handleOpen() {
        this.setState({
            modalOpen: true
        })
    }
    handleClose() {
        this.setState({
            modalOpen: false
        })
    }
    csvToSourceVector(file) {
        const fr = new FileReader()
        fr.onload = (evt) => {
            const vectorData = evt.target.result
            const config = {
                delimiter: "",	// auto-detect
                newline: "",	// auto-detect
                quoteChar: '"',
                header: true,
                dynamicTyping: false,
                preview: 0,
                encoding: "",
                worker: false,
                comments: false,
                step: undefined,
                complete: undefined,
                error: undefined,
                download: false,
                skipEmptyLines: false,
                chunk: undefined,
                fastMode: undefined,
                beforeFirstChunk: undefined,
                withCredentials: undefined
            }
            const map = this.props.map
            const currentProj = map.getView().getProjection()

            const source = new ol.source.Vector()
            let sourceFormat = new ol.format.GeoJSON()
            const dataParse = Papa.parse(vectorData, config)
            const csvGeoJson = GeoJSON.parse(dataParse.data, { Point: ['y', 'x'] })
            const projections = {
                dataProjection: 'EPSG:4326',
                featureProjection: 'EPSG:3857'
            };

            source.addFeatures(sourceFormat.readFeatures(JSON.stringify(csvGeoJson), projections))
            this.addLayerToMap(source)

        }
        fr.readAsText(file)

    }
    shapeToSourceVector(file){
        const fs = new FileReader()
        const sourceFormat = new ol.format.GeoJSON()
        const source = new ol.source.Vector()
        fs.onload = (evt)=>{
            const vectorData = evt.target.result
            const projections = {
                dataProjection: 'EPSG:4326',
                featureProjection: 'EPSG:3857'
            };
            shp(vectorData).then((geojson)=>{
                source.addFeatures(sourceFormat.readFeatures(geojson,projections))
                this.addLayerToMap(source)
            })

        }
        fs.readAsArrayBuffer(file)

    }
    addLayerToMap(source) {
        const map = this.props.map
        var numfeatures = source.getFeatures().length;
        const nombrelayer = this.state.formData.nombre
        if (numfeatures > 0) {
            alertify.confirm("Importacion de vectores", "La fuente de datos <b>" + nombrelayer + "</b> contiene <b>" + numfeatures + "</b> elementos. " +
                "¿Desea agregar la capa al mapa?",
                function () {

                    const vectorLayer = crearVectorLayer(source, nombrelayer + " (" + numfeatures + ")")
                    map.addLayer(vectorLayer)
                    map.getView().fit(source.getExtent(), map.getSize())
                    return this;

                },
                function () {
                    alertify.message("No se añadio la capa.");

                })

        } else {
            alertify.message("La fuente de datos <b>" + nombrelayer + "</b> contiene <b>" + numfeatures + "</b> elementos. No se añadirá ningun dato.");

        }

    }
    handleSubmitForm(formData) {
        this.setState({
            formData: formData
        })
        this.handleClose()
        /*
        formData = {nombre,file,formato}
        */

        switch (formData.formato) {
            case "csv":
                this.csvToSourceVector(formData.file)
                break
            case "shapefile":
                this.shapeToSourceVector(formData.file)
                break

        }


    }

    render() {
        return (<Modal
            trigger={<RaisedButton label="Shp,csv" icon={<i class="fa fa-upload" aria-hidden="true"></i>} onClick={this.handleOpen.bind(this)}/>}
            open={this.state.modalOpen}
            onClose={this.handleClose.bind(this)}
            size='small'
        >
            <Header icon='browser' content='Importar Datos' />
            <Modal.Content>
                <FormAddLayer onHandleSubmit={this.handleSubmitForm.bind(this)} />
            </Modal.Content>

        </Modal>)
    }

}

export default AddLayerLS;
