import React from 'react';
import { Button, Header, Modal, Icon } from 'semantic-ui-react'
import {connect} from 'react-redux'
import store from '../../store/store'
import axios from 'axios'
import ol from 'openlayers'
import ListServices from './AddLayerServices/ListServices.jsx'


class AddLayerServices extends React.Component {
  constructor() {
    super();
    this.state = { 
      modalOpen: false,
      services:[]
     };
  }
  handleOpen(){
    this.setState({
      modalOpen:true
    })
  }
  handleClose(){
    this.setState({
      modalOpen:false
    })
  }
  cargarServicios(){
    const url = "http://gisnovomap.com/geoserver/wms"
    const params = {
      service:"wms",
      version:  "1.3.0",
      request:"GetCapabilities",
      namespace:"MINSA"
    }
    const auth = {
      username:"MINSA_Admin",
      password:"gj78rt96"
    }
    const auth2 = {
      username:"gliberte",
      password:"bDT-zeY-cn8-APG"
    }
    const config = {
      url:url,
      params:params
    }
    const format = new ol.format.WMSCapabilities()
    axios(config)
    .then((response)=>{
      const resultado = format.read(response.data)
      const services = resultado.Capability.Layer.Layer
      console.log(services)
      this.setState({
        services:services
      })
    })
    .catch((err)=>{
      console.log(err)
    })

  }
  render() {
    return (
      <Modal
            trigger={<Button onClick={this.handleOpen.bind(this)}><Icon name="map outline" color="yellow" /></Button>}
            open={this.state.modalOpen}
            onClose={this.handleClose.bind(this)}
            size='small'
        >
            <Header icon='browser' content='Importar Datos' />
            <Modal.Content>
                <Button onClick={this.cargarServicios.bind(this)}>Cargar Servicios</Button>
                <ListServices services={this.state.services}/>
            </Modal.Content>
        </Modal>
    )
  }

}

export default AddLayerServices;
