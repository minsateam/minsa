import React from 'react';
import Draggable from 'react-draggable'
import { Button, Header, Icon, Card, Dropdown, Divider, Grid, Label, Form, Segment, Checkbox, Input } from 'semantic-ui-react'
import { css } from 'glamor'
import { BarChart, LineChart, Bar, XAxis, YAxis, CartesianGrid, Line, Tooltip, Legend, PieChart, Pie } from 'recharts'
import ol from 'openlayers'
import { getPositionForFeature } from '../../services/geoprocesos'
import FlatButton from 'material-ui/FlatButton'

class Graficas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: "hidden",
            layerSelected: null,
            tipoGraficaSelected: "",
            optionsCampos: [],
            campoNumerico: "",
            campoClase: "",
            data: [],
            layerResalto: null,
            useSelect: false
        };
    }
    listaLayersOptions() {
        const layersDelMapa = this.props.listaLayers
        if (layersDelMapa.length > 0) {
            const layersTipoVector = layersDelMapa.filter((layer) => layer.get('tipo') == "vector")
            return layersTipoVector.map((layer) => {
                return {
                    key: layer.get('id'),
                    text: layer.get('nombre'),
                    value: layer
                }
            })
        }
    }

    onChangeLayer(e, data) {
        const layerSelected = data.value
        const propiedadesDelLayerSelected = layerSelected.get('headers')
        this.setState({
            layerSelected: layerSelected
        })
        const optionsCampos = []
        for (const p in propiedadesDelLayerSelected) {
            optionsCampos.push({
                text: p,
                key: p,
                value: p
            })
        }
        this.setState({
            optionsCampos: optionsCampos
        })
    }
    tiposGraficosOptions() {
        return [
            {
                text: "Linea",
                value: "linea",
                key: "linea"
            },
            {
                text: "Barras",
                value: "barras",
                key: "barras"
            },
            {
                text: "Pastel",
                value: "pastel",
                key: "pastel"
            }
        ]
    }
    onChangeTipoGrafico(e, data) {
        this.setState({
            tipoGraficaSelected: data.value
        })
    }
    onChangeCampo(e, data) {
        if (data.name == "campoNumerico") {
            this.setState({
                campoNumerico: data.value
            })

        } else {
            this.setState({
                campoClase: data.value
            })
        }




    }
    onChangeCampoClase(e, data) {
        console.log(data.value)
        this.setState({
            campoClase: data.value
        })

    }
    onMouseMove(data, index) {
        const map = this.props.map
        let layerResalto = new ol.layer.Vector()
        Object.assign(layerResalto, this.props.layerResalto)
        const interaction = this.props.selectInteraction


        let feature
        if (data.isTooltipActive) {



            const idFeature = data.activePayload[0].payload.idObject

            if (this.state.layerSelected) {
                const layerSource = this.state.layerSelected.getSource()
                feature = layerSource.getFeatureById(idFeature)


            } else {
                feature = this.props.selectedFeatures.find((f) => f.getId() == idFeature)
            }
            let highlight = layerResalto.getSource().getFeatures()[0]
            if(highlight){
              if (feature.getId() !== highlight.getId()) {

                      layerResalto.getSource().removeFeature(highlight);
                      layerResalto.getSource().addFeature(feature);


              }
            }else{
              if (feature) {

                  layerResalto.getSource().addFeature(feature);
              }
            }

        }else{
            interaction.getLayer().setOpacity(0.7)
        }
    }
    onClickPie(data) {

        const map = this.props.map
        const layerResalto = this.state.layerResalto

        const layerSource = this.state.layerSelected.getSource()
        layerResalto.getSource().clear()

        const idFeature = data.payload.idObject
        console.log(idFeature)
        const feature = layerSource.getFeatureById(idFeature)


        let highlight;


        if (feature !== highlight) {
            if (highlight) {
                layerResalto.getSource().removeFeature(highlight);
            }
            if (feature) {
                console.log("agregando")
                layerResalto.getSource().addFeature(feature);
            }
            highlight = feature;
        }

    }
    onClickChart(data) {
        if (data) {
            const idFeature = data.activePayload[0].payload.idObject
            const map = this.props.map
            let feature
            if (this.state.layerSelected) {
                const selectedLayerSource = this.state.layerSelected.getSource()
                feature = selectedLayerSource.getFeatureById(idFeature)

            } else {
                feature = this.props.selectedFeatures.find((f) => f.getId() == idFeature)
            }

            const position = getPositionForFeature(feature.getGeometry())
            
            map.getView().animate({center:position})
        }
    }
    renderGrafica() {
        switch (this.state.tipoGraficaSelected) {
            case "":
                return (<p>Seleccione un tipo de grafico</p>)
                break
            case "linea":
                return (<LineChart
                    width={600}
                    height={300}
                    data={this.state.data}
                    onMouseMove={this.onMouseMove.bind(this)}
                    onClick={this.onClickChart.bind(this)}


                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                    <XAxis dataKey="clase" />
                    <YAxis />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" dataKey="valor" stroke="#8884d8" activeDot={{ r: 8 }} />

                </LineChart>)
                break
            case "barras":
                return (
                    <BarChart
                        width={600}
                        height={300} data={this.state.data}
                        onMouseMove={this.onMouseMove.bind(this)}
                        onClick={this.onClickChart.bind(this)}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                        <XAxis dataKey="clase" />
                        <YAxis />
                        <CartesianGrid strokeDasharray="3 3" />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="valor" fill="#8884d8" />
                    </BarChart>)
                break
            case "pastel":
                return (
                    <PieChart width={600} height={300}>
                        <Pie nameKey="clase" valueKey="valor" onClick={this.onClickPie.bind(this)} data={this.state.data} cx={200} cy={200} innerRadius={70} outerRadius={90} fill="#82ca9d" label />
                    </PieChart>
                )
                break
        }
    }
    useSelected(e, data) {
        console.log(data)
        this.setState({
            useSelect: data.checked
        })
        if (data.checked) {
            const selectedFeatures = this.props.selectedFeatures
            const headers = selectedFeatures[0].getProperties()
            delete headers.geom
            console.log(headers)
            const optionsCampos = []
            for (const p in headers) {
                optionsCampos.push({
                    text: p,
                    key: p,
                    value: p
                })
            }
            this.setState({
                optionsCampos: optionsCampos
            })

        }

    }

    actualizarGrafica() {
        let features
        if (this.state.useSelect) {
            features = this.props.selectedFeatures
        } else {
            const layerSelected = this.state.layerSelected
            features = layerSelected.getSource().getFeatures()
        }

        const thedata = features.map((feature) => {
            if (this.state.campoClase != "") {
                return {
                    valor: +feature.get(this.state.campoNumerico),
                    clase: feature.get(this.state.campoClase),
                    idObject: feature.getId()
                }

            } else {
                return {
                    valor: +feature.get(this.state.campoNumerico)
                }
            }

        })
        this.setState({
            data: thedata
        })

    }

    onCerrarGrafico() {
        const layerResalto = this.props.layerResalto
        if (layerResalto) {
            layerResalto.getSource().clear()
        }
        this.setState({
            isVisible: 'hidden'
        })
    }
    onAbrirGraficos() {
        this.setState({
            isVisible: "visible"
        })
    }

    render() {
        const rule = css({
            position: "absolute",
            top: "130%",
            left: "-105%",
            transform: "translate(-50%, -50%)",
            backgroundColor: "white",
            zIndex: 10,
            visibility: this.state.isVisible
        })
        const ruleGraph = css({
            position: "absolute",
            top: "130%",
            left: "40%",
            backgroundColor: "white",
            minWidth: "300px",
            zIndex: 10,
            visibility: this.state.isVisible,
            padding: "20px"
        })
        const listaLayersOptions = this.listaLayersOptions()
        const tiposGraficosOptions = this.tiposGraficosOptions()
        const renderGrafica = this.renderGrafica()
        return (
            <div>
                <FlatButton onClick={this.onAbrirGraficos.bind(this)}
                label="Graficos" icon={<i
                class="fa fa-line-chart"
                ria-hidden="true"></i>}
                primary={true}
                 />
                <Draggable>
                    <div {...rule}>
                        <Card>
                            <Card.Content>
                                <Card.Header as="h3">
                                    <Icon name="bar chart" />
                                    Grafico
                                    <Button onClick={this.onCerrarGrafico.bind(this)} size="mini" floated="right" icon="remove" />

                                </Card.Header>

                                <Label as='a' color='teal' tag>{this.props.selectedFeatures.length} seleccionados</Label>

                                <Form.Field disabled>
                                    <Checkbox onChange={this.useSelected.bind(this)} checked={this.state.useSelect} label='Usar seleccion actual' />
                                </Form.Field>




                                <Divider />
                                <Dropdown
                                    placeholder="Seleccionar una Capa"
                                    search floating labeled button className='icon'
                                    options={listaLayersOptions}
                                    onChange={this.onChangeLayer.bind(this)}
                                />
                                <Divider />
                                <Dropdown
                                    placeholder="Seleccionar un tipo de Grafico"
                                    search floating labeled button className="icon"
                                    options={tiposGraficosOptions}
                                    onChange={this.onChangeTipoGrafico.bind(this)}
                                >
                                </Dropdown>
                                <Divider />
                                <Header as="h4">Series</Header>
                                <Grid columns="equal">
                                    <Grid.Column>
                                        <span>
                                            Campo Numerico
                                            {' '}
                                            <Dropdown
                                                scrolling
                                                name="campoNumerico"
                                                inline options={this.state.optionsCampos}
                                                onChange={this.onChangeCampo.bind(this)}
                                            />
                                        </span>


                                    </Grid.Column>
                                    <Grid.Column>
                                        <span>
                                            Clases o Agrupaciones
                                            {' '}
                                            <Dropdown
                                                scrolling
                                                name="campoClase"
                                                inline options={this.state.optionsCampos}
                                                onChange={this.onChangeCampo.bind(this)}
                                            />
                                        </span>
                                    </Grid.Column>
                                </Grid>
                            </Card.Content>
                            <Card.Content extra>
                                <Button inverted fluid color="blue" onClick={this.actualizarGrafica.bind(this)}>Actualizar Grafica</Button>

                            </Card.Content>

                        </Card>

                    </div>

                </Draggable>
                <Draggable>
                    <div {...ruleGraph}>
                        <Header as='h3'>
                            <Icon name='area chart' />
                            <Header.Content>
                                Grafica
                            </Header.Content>
                        </Header>

                        {renderGrafica}

                    </div>
                </Draggable>
            </div>
        )
    }

}
export default Graficas
