import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem'
import FormNuevoUsuario from './NuevoUsuario/FormNuevoUsuario.jsx'

//actions 
import store from '../../store/store'
import {openFormNuevoUsuario} from '../../actions/appActions'

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class NuevoUsuario extends React.Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    store.dispatch(openFormNuevoUsuario())
  };

  handleClose = () => {
    store.dispatch(openFormNuevoUsuario())
  };

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        disabled={true}
        onTouchTap={this.handleClose}
      />,
    ];

    return (
      <div>
        <Dialog
          title="Crear Nuevo Usuario"
          //actions={actions}
          modal={true}
          autoScrollBodyContent={true}
          open={this.props.open}
        >
        <FormNuevoUsuario />
          
        </Dialog>
      </div>
    );
  }
}