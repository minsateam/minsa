import React from 'react';

class ListServices extends React.Component {
  constructor() {
    super();
    this.state = { someKey: 'someValue' };
  }

  render() {
    return (
        <ul>
            {this.props.services.map((service)=>
                <li key={service.Name}>{service.Title}</li>
            )}
        </ul>
    )
  }


}

export default ListServices;
