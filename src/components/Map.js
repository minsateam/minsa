import React from 'react';
import ReactDOMServer from 'react-dom/server'
import ol from 'openlayers'
import { connect } from 'react-redux'
import store from '../store/store'
import LayerSwitcher from './map/LayerSwitcher.js'
//import css from './map/LayerSwitcher.css'
import "./map/LayerSwitcher.css"
import './ol.css'
//Importando accines al Mapa
import { setMap, changeLayers, moverLayerArriba, moverLayerAbajo, getCapabilities, setLayerResalto, setSelectInteraction, addSnap, setFeatureResaltado, spinerOn, spinerOff } from '../actions/mapActions'
import { openTable, closeTable, updateTable, setLayerTable } from '../actions/tableActions'
import { showFeatureInfoWms } from '../actions/layerActions'
import {snackBarOn} from '../actions/appActions'

import '../../node_modules/openlayers/dist/ol-debug.css'
import { css } from 'glamor'
import LayerTree from './LayerTree.jsx'
import './map/olpopup.css'
import './map/olpopup.js'
import { Table } from 'semantic-ui-react'
import { crearVectorServiceByBbox, servicioDinamicoPorExtencion } from '../services/geoprocesos'
import './map.css'


ol.layer.Vector.prototype.buildHeaders = function () {
    var headers = this.get('headers') || {};
    var features = this.getSource().getFeatures();
    for (var i = 0; i < features.length; i += 1) {
        {
            var attributes = features[i].getProperties();
            for (var j in attributes) {
                if (typeof attributes[j] !== 'object' && !(j in headers)) {
                    if (typeof attributes[j] == 'string') {
                        headers[j] = 'string';
                    } else if (typeof attributes[j] == 'number') {
                        headers[j] = 'number';
                    }
                }
            }
        }
    }
    this.set('headers', headers);
    return this;
};

@connect((store) => {
    return {
        map: store.map.map,
        layers: store.map.layers,
        layertree: store.layertree,
        selectInteraction: store.map.selectInteraction,
        isEditing: store.app.isEditing,
        layerTable: store.table.layerTable
    }


})
class Map extends React.Component {
    constructor(props) {
        super(props);

    }

    makeLayerBases() {



        const spinerFunctionOn = () => {
            store.dispatch(spinerOn())
        }
        const spinerFunctionOff = () => {
            store.dispatch(spinerOff())
        }


        function bingMapLayer(tipo, visible) {
            var bingMapLayer = new ol.layer.Tile({
                name: "BingMap" + " (" + tipo + ")",
                title: "BingMap" + " (" + tipo + ")",
                type: 'base',
                source: new ol.source.BingMaps({
                    key: 'AhH6Hkq6-CKdL8ma30smyvA5Z8vsNLba-leIdtTFbFM1gJI4Vyy5nh1gEZexQtxg',
                    imagerySet: tipo
                }),
                visible: visible
            });
            //bingMapLayer.getSource().on('tileloadstart', spinerFunctionOn)
            //bingMapLayer.getSource().on('tileloadend', spinerFunctionOff)
            return bingMapLayer;
        }


        const bingMapLayerRoad = bingMapLayer("Road", true);
        const bingMapLayerAereal = bingMapLayer("Aerial", false);
        const bingMapLayerAerialWithLabels = bingMapLayer("AerialWithLabels", false);

        const openStreetMapLayer = new ol.layer.Tile({
            name: "OpenStreetMap",
            title: "OpenStreetMap",
            type: "base",
            visible: true,
            source: new ol.source.OSM()
        })
        //openStreetMapLayer.getSource().on('tileloadstart', spinerFunctionOn)
        //openStreetMapLayer.getSource().on('tileloadend', spinerFunctionOff)

        const stamenLayer = new ol.layer.Tile({
            name: "Stamen Terreno",
            title: "Stamen Terreno",
            type: "base",
            visible: true,
            source: new ol.source.Stamen({
                layer: 'terrain'
            })
        })
        //stamenLayer.getSource().on('tileloadstart', spinerFunctionOn)
        //stamenLayer.getSource().on('tileloadend', spinerFunctionOff)

        const mapBoxLayer = new ol.layer.Tile({
            name: "MapBox Satellite",
            title: "MapBox Satellite",
            type: "base",
            visible: false,
            source: new ol.source.XYZ({
                url: "https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ2xpYmVydGUiLCJhIjoiY2l5OTFjMGVtMDA3aDMycW91eDZ2bnBkMyJ9.0ribIg_tlpagcxJmFyFM9A",
                crossOrigin: 'anonymous'

            })
        })
        //mapBoxLayer.getSource().on('tileloadstart', spinerFunctionOn)
        //mapBoxLayer.getSource().on('tileloadend', spinerFunctionOff)

        const layerBases = new ol.layer.Group({
            title: "Mapas Base",
            layers: [
                openStreetMapLayer,
                stamenLayer,
                mapBoxLayer,
                bingMapLayerAereal,
                bingMapLayerAerialWithLabels,
                bingMapLayerRoad
            ]
        });

        return layerBases

    }
    initMap() {

        const layerBases = this.makeLayerBases()
        const layerSwitcher = new LayerSwitcher()

        const elevation = new ol.source.Raster({
            sources: [new ol.source.XYZ({
                url: "https://api.mapbox.com/v4/mapbox.terrain-rgb/{z}/{x}/{y}.pngraw?access_token=pk.eyJ1IjoiZ2xpYmVydGUiLCJhIjoiY2l5OTFjMGVtMDA3aDMycW91eDZ2bnBkMyJ9.0ribIg_tlpagcxJmFyFM9A"
            })]
        })
        const layerElevation = new ol.layer.Image({
            source: elevation,
            name: "Elevacion",
            title: "Elevacion"
        })
        const center = ol.proj.fromLonLat([-79.504571, 9.034881])

        const map = new ol.Map({
            target: 'map',
            layers: [
                layerBases,

                //googleLayer
            ],
            view: new ol.View({
                center: center,
                zoom: 8
            }),
            controls: [
                layerSwitcher,
                new ol.control.Zoom(),
                new ol.control.Rotate(),
                new ol.control.Attribution(),
                new ol.control.ZoomSlider(),
                new ol.control.MousePosition({
                    coordinateFormat(coords){
                        const lat = coords[1].toFixed(3)
                        const lon = coords[0].toFixed(3)
                        return `lat: ${lat}, lon: ${lon}`
                    },
                    projection:"EPSG:4326"
                }),
                new ol.control.ScaleLine({
                  minWidth:128
                }),
                new ol.control.OverviewMap()
            ],

        })

        //InfoWindows:
        const infoWindow = new ol.Overlay.Popup()
        map.addOverlay(infoWindow)
        this.props.dispatch(setMap(map))

        /*
        Eventos de Mapa
        */

        map.getLayers().on('add', function (evt) {
            const layer = evt.element
            if (layer instanceof ol.layer.Vector) {
                layer.set('statusEdicion', false)
                if (layer.get('origen') != "wfs") {
                    layer.buildHeaders()
                    layer.getSource().on('change', () => {
                        layer.buildHeaders()
                    })

                }

                this.props.dispatch(changeLayers(layer))

            } else {
                if (layer.get('isRemovable')) {
                    layer.buildHeaders()
                }
                this.props.dispatch(changeLayers(layer))

            }

        }, this)
        map.getLayers().on('remove', function (evt) {
            this.props.dispatch(changeLayers(evt.element))
            this.props.dispatch(closeTable(evt.element.get('id')))
        }, this)

        map.on('click', (evt) => {
            const pixel = evt.pixel

            map.forEachFeatureAtPixel(pixel, (feature) => {
                const props = feature.getProperties()
                const content = this.crearInfoWindowFeature(props)
                const geom = feature.getGeometry()
                let position

                switch (geom.getType()) {
                    case 'MultiPolygon':
                        var poly = geom.getPolygons().reduce(function (left, right) {
                            return left.getArea() > right.getArea() ? left : right;
                        });
                        position = poly.getInteriorPoint().getCoordinates();
                        break;
                    case 'Polygon':
                        position = geom.getInteriorPoint().getCoordinates();
                        break;
                    case 'LineString':
                        position = geom.getCoordinateAt(0.5)
                        break
                    case 'MultiLineString':
                        console.log("multiline")
                        const line = geom.getLineStrings().reduce(function (left, right) {
                            return left.getLength() > right.getLength() ? left : right
                        })
                        position = line.getCoordinateAt(0.5)
                        break
                    case 'Point':
                        position = geom.getCoordinates()
                }

                infoWindow.show(position, content)
            }, {
                    layerFilter: (layerCandidate) => {

                        return (layerCandidate.get('tipo') == "vector" && (layerCandidate.get('nombre') != "linea del perfil")
                        )

                    }
                }

            )
        })

        this.setFeatureOverlay(map)
        this.setSelectingFeatures(map)

    }

    setSelectingFeatures(map) {
        const styleConfig = {
            fill: new ol.style.Fill({
                color: 'rgba(0,255,255,0.8)'
            }),
            stroke: new ol.style.Stroke({
                color: '#FF4D00',
                width: 3
            })

        }
        const image = new ol.style.Circle({
            fill: styleConfig.fill,
            stroke: styleConfig.stroke,
            radius: 9
        })
        const seleccion = new ol.interaction.Select({
            style: [new ol.style.Style({ ...styleConfig, image: image })]
        })
        seleccion.setActive(false)

        map.addInteraction(seleccion)
        this.props.dispatch(setSelectInteraction(seleccion))

    }
    crearInfoWindowFeature(props) {
        console.log(props)
        const content = function (props) {
            const arrayContent = []
            for (const p in props) {
                if (p != "geometry" && p != "geom" && p != "_proto_" && p != 'simbol') {
                    if (p === "ruta_pdf") {
                        arrayContent.push(
                            <Table.Row>
                                <Table.HeaderCell>{p}</Table.HeaderCell>
                                <Table.Cell><a href={props[p]} target="_blank">{props[p]}</a></Table.Cell>

                            </Table.Row>
                        )

                    }else {
                        arrayContent.push(
                            <Table.Row>
                                <Table.HeaderCell>{p}</Table.HeaderCell>
                                <Table.Cell>{props[p]}</Table.Cell>

                            </Table.Row>
                        )
                    }

                }
            }
            return arrayContent
        }

        return ReactDOMServer.renderToString(
            <div>
                <h4>Propiedades:</h4>
                <Table>
                    <Table.Body>
                        {content(props)}
                    </Table.Body>
                </Table>
            </div>
        )
    }
    setFeatureOverlay(map) {
        const styleConfig = {
            fill: new ol.style.Fill({
                color: 'rgba(255,0,0,0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#FF0000',
                width: 4
            })

        }
        const image = new ol.style.Circle({
            fill: styleConfig.fill,
            stroke: styleConfig.stroke,
            radius: 8
        })
        map.on("pointermove", (evt) => {
            if (evt.draggin) {
                return
            }
            const pixel = map.getEventPixel(evt.originalEvent)
            if (!this.props.isEditing.status) {
                displayFeatureInfo(pixel);
            }

        })
        const featureOverlay = new ol.layer.Vector({
            source: new ol.source.Vector(),
            map: map,
            style: [
                new ol.style.Style({ ...styleConfig, image: image })
            ]
        });
        featureOverlay.getSource().on('addfeature', (evt) => {
            this.props.dispatch(setFeatureResaltado(evt.feature))
            this.props.dispatch(updateTable(this.props.layerTable))

        })
        this.props.dispatch(setLayerResalto(featureOverlay))

        const displayFeatureInfo = function (pixel) {
            let highlight = featureOverlay.getSource().getFeatures()[0]
            var feature = map.forEachFeatureAtPixel(pixel, (feature) => {
                return feature;
            }
            );
            if (highlight) {
                if (feature) {

                    if (feature.getId() !== highlight.getId()) {

                        featureOverlay.getSource().removeFeature(highlight);
                    }
                } else {
                    featureOverlay.getSource().clear()
                }
            }
            if (feature) {
                document.body.style.cursor = 'pointer';
                featureOverlay.getSource().addFeature(feature);
            } else {
                document.body.style.cursor = 'default';
            }
        };
    }
    showFeatureInfoWms = (featureurl) => {
        this.props.dispatch(showFeatureInfoWms(featureurl))
    }
    openTable(layer) {
        this.props.dispatch(openTable(layer))
        this.props.dispatch(setLayerTable(layer))
    }
    componentDidMount() {
        this.initMap()
        this.props.dispatch(getCapabilities())
    }
    onMoverLayerArriba(layer) {
        const layers = this.props.map.getLayers().getArray()
        const index = layers.indexOf(layer)
        const limit = layers.length - index
        console.log(limit)
        if (index > 0 && limit > 1) {
            layers.move(index, index + 1)
        }
        this.props.map.render()
        this.props.dispatch(changeLayers())
    }
    onMoverLayerAbajo(layer) {
        const layers = this.props.map.getLayers().getArray()
        const index = layers.indexOf(layer)

        if (index > 1) {
            layers.move(index, index - 1)
        }
        this.props.map.render()
        this.props.dispatch(changeLayers())

    }
    onServicioDinamicoPorExtension(layername) {
        const map = this.props.map
        const bbox = map.getView().calculateExtent(map.getSize())
        const layer = servicioDinamicoPorExtencion(layername)

        map.addLayer(layer)

    }
    onSolicitarVectores(layer) {

        const map = this.props.map
        const bbox = map.getView().calculateExtent(map.getSize())
        const vectorlayer = crearVectorServiceByBbox(layer, bbox, map.getView().getResolution())

        map.addLayer(vectorlayer)

    }
    render() {
        const rule = css({
            height: "calc(100%-120px)",
            position: "relative"
        })
        return <div id="map">

            <LayerTree
                onOpenTable={this.openTable.bind(this)}
                isOpen={this.props.layertree.isVisible}
                layers={this.props.layers}
                map={this.props.map}
                selectInteraction={this.props.selectInteraction}
                onMoverLayerArriba={this.onMoverLayerArriba.bind(this)}
                onMoverLayerAbajo={this.onMoverLayerAbajo.bind(this)}
                onSolicitarVectores={this.onSolicitarVectores.bind(this)}
                showFeatureInfoWms={this.showFeatureInfoWms}
                spinerOn={()=>this.props.dispatch(spinerOn())}
                spinerOff={()=>this.props.dispatch(spinerOff())}
                snackBarOn={(mensaje)=>this.props.dispatch(snackBarOn(mensaje))}
            />

        </div>;
    }

}

export default Map;
