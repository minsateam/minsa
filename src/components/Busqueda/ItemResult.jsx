import React from 'react'
import ol from 'openlayers'
import {Item} from 'semantic-ui-react'
import {css} from 'glamor'
import {addItemResultToMap} from '../../services/geoprocesos'

const ItemResult = ({feature,typeName,value}) => {
    
    const arrayIdData = feature.getId().split('.')
    const id = arrayIdData[1]
    const header = "valor: "+value
    const description = "Capa: "+typeName

    const rule = css({
        padding:"5px",
        ' :hover':{
            background:"#D6E5FF",
            cursor:"pointer"
        },
        margin:"3px 0 3px 0",
        ' p':{
            margin:0
        },
        ' span':{
            fontFamily: "Times New Roman, Times, serif",
            fontStyle: "italic",
            color:"#8B8D8F"
        }
    })
    const onClickItem = (feature)=>{
        addItemResultToMap(feature)
    }

    return (
        <li {...rule} onClick={()=>onClickItem(feature)}>
            <div>
                <h3>{header}</h3>
                <p>ObjectId: {id}</p>
                <span>{description}</span>
            </div>
        </li>
    )
}

ItemResult.propTypes = {
    feature: React.PropTypes.instanceOf(ol.Feature),
    typeName:React.PropTypes.string,
    value:React.PropTypes.any
}

export default ItemResult