import React, { PropTypes } from 'react';
import { Grid, Header, Item } from 'semantic-ui-react'
import camposbusqueda from '../../services/camposbusqueda'
import { busqueda } from '../../services/geoprocesos'
import store from '../../store/store'
import { clearResults, endSearch} from '../../actions/searchActions'
import { css } from 'glamor'
import ItemResult from './ItemResult.jsx'
import { spinerOn, spinerOff } from '../../actions/mapActions'
import Badge from 'material-ui/Badge';
import Paper from 'material-ui/Paper'
import IconButton from 'material-ui/IconButton'
import SearchIcon from 'material-ui/svg-icons/action/search'
import TextField from 'material-ui/TextField'

import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import './animSearchItemResults.css'


class Busqueda extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      value: "",
      countResult: 0
    };
  }
  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })
  handleResultSelect = (e, result) => this.setState({ value: result.title })

  handleSearchChange = (e) => {
    const { value } = e.target

    this.setState({ value })

  }

  onBuscar = (e) => {
    store.dispatch(endSearch(false))
    e.preventDefault()
    store.dispatch(clearResults())
    this.setState({
      isLoading: true
    })
    
    camposbusqueda.forEach((item) => {
      if (this.state.value) {
        
        busqueda(item.typeName, item.campo, this.state.value)
        //busqueda("MINSA:HS_PMCamaras_1","CIN","ra-63al")
        
      } else {
        busqueda(item.typeName, item.campo)
      }
  
    })
    
  
    store.dispatch(endSearch(true))

  }
  limpiarResultados = () => {
    store.dispatch(clearResults())
  }

  listResult = () => {
    const style = {
      padding: "12px",
      zIndex: 11
    }
    if (this.props.endSearch) {
      if (this.props.searchResults.length > 0) {

        return (
          <Badge
            badgeContent={this.props.countResult}
            primary={true}>

            <ReactCSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={1500}
              transitionLeaveTimeout={700}>
              {this.props.searchResults.map((result) => {
                return (
                  <ItemResult
                    key={result.feature.getId()}
                    feature={result.feature}
                    typeName={result.typeName}
                    value={result.value}
                  />

                )
              })
              }
            </ReactCSSTransitionGroup>



          </Badge>
        )

      } else {
        if(this.state.value){
          return (
          <li>
            <Paper style={style} zDepth={4}>
              Sin Resultados
            </Paper>
          </li>)
        }else{
          return null
        }
        
          

        

      }


    }


  }

  render() {
    const rule1 = css({
      position: "relative"
    })
    const style = {
      padding: "12px",
      position: "absolute",
      zIndex: 13,
      top: "90%"
    }
    const rule2 = css({
      position: "absolute",
      top: "110%",
      overflow: "auto",
      maxHeight: "500px",
      zIndex: 12,
      listStyle: "none",
      paddingLeft: 0,
      ' li': {
        background: "white"

      }
    })
    const { isLoading, value } = this.state

    const listResult = this.listResult()

    return (
      <div {...rule1}>
        <form onSubmit={this.onBuscar}>


          <TextField
            hintText="texto a buscar..."
            onChange={this.handleSearchChange}
            value={value}
          />
          <IconButton tooltip="Iniciar Busqueda" type='submit'>
            <SearchIcon />
          </IconButton>


        </form>

        <ul {...rule2}>

          {listResult}



        </ul>
        {this.props.searchResults.length > 0 &&
          <Paper style={style} zDepth={4}>
            <a href="#" onClick={this.limpiarResultados}>Limpiar Resultados</a>

          </Paper>
        }





      </div>


    )
  }


}

export default Busqueda;
