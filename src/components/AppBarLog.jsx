import React, { Component } from 'react';
import ol from 'openlayers'
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import Logo from '../img/favicon.png'
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Parse from 'parse'
import store from '../store/store'
import { currentUser } from '../actions/userActions'
import { openFormNuevoUsuario, openFormBookmarks } from '../actions/appActions'
import { spinerOn, spinerOff } from '../actions/mapActions'
import { NotificationManager } from 'react-notifications'



const salir = () => {
    store.dispatch(spinerOn())
    Parse.User.logOut().then(() => {
        store.dispatch(spinerOff())
        store.dispatch(currentUser(null))
        NotificationManager.info('Session finalizada')

    })
}
const nuevoUsuario = () => store.dispatch(openFormNuevoUsuario())
const bookmarks = () => {
    store.dispatch(openFormBookmarks())
}
const esAdmin = ()=>{
  return Parse.User.current() && Parse.User.current().get('esAdmin')
}


const Logged = (props) => (
    <IconMenu
        {...props}
        iconButtonElement={
            <IconButton><MoreVertIcon /></IconButton>
        }
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
    >

        {esAdmin() && <MenuItem primaryText="Nuevo Usuario" onTouchTap={nuevoUsuario} />}
        <MenuItem primaryText="Bookmarks" onTouchTap={bookmarks} />
        <MenuItem primaryText="Salir" onTouchTap={salir} />
    </IconMenu>
);

Logged.muiName = 'IconMenu';

/**
 * This example is taking advantage of the composability of the `AppBar`
 * to render different components depending on the application state.
 */
class AppBarExampleComposition extends Component {

    onClickTitulo = () => {
        this.props.map.getView().animate({
            center: ol.proj.fromLonLat([-79.504571, 9.034881]),
            zoom: 8
        })
    }




    render() {
        const styles = {
            titulo: {
                cursor: "pointer"
            }
        }
        return (
            <div>

                <AppBar
                    onTitleTouchTap={this.onClickTitulo}
                    title={<span title="toque para retornar a la vista inicial" style={styles.titulo}>PMAPS</span>}
                    zDepth={3}
                    iconElementLeft={<img src={Logo} width={25} height={25} style={{marginTop:"10px"}} />}
                    iconElementRight={this.props.logged ? <Logged /> : null}
                />
            </div>
        );
    }
}

export default AppBarExampleComposition;
