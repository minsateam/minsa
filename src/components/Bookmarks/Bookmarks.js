import React, { PropTypes } from 'react'
import AppBar from 'material-ui/AppBar'
import './Bookmarks.css'
import classNames from 'classnames'
import Divider from 'material-ui/Divider'
import CrearBookmark from './CrearBookmark'
import BookmarksList from './BookmarksList'
import {openFormBookmarks} from '../../actions/appActions'
import {guardarNuevoBookmark,borrarBookmark,editarBookmark} from '../../actions/bookmarkActions'
import store from '../../store/store'
import IconCerrar from 'material-ui/svg-icons/navigation/close'
import IconButton from 'material-ui/IconButton';

class Bookmarks extends React.Component{

  irABookmark = (extent) => {
    const map = this.props.map
    map.getView().fit(extent,{
      duration:1000
    })
  }

  onClickIrBookmark = (bookmark) => {
    this.irABookmark(bookmark.get('extent'))
  }
  onClickBorrarBookmark = (bookmark) => {
    store.dispatch(borrarBookmark(bookmark))
  }
  onClickEditarBookmark = (bookmark) => {
    const map = this.props.map
    const currentExt = map.getView().calculateExtent(map.getSize())
    bookmark.set('extent',currentExt)
    store.dispatch(editarBookmark(bookmark))
  }
  onSubmitNuevoBookmark = (descripcion) => {
    this.crearNuevoBookmark(descripcion)
  }
  crearNuevoBookmark = (descripcion) => {
    const map = this.props.map
    const currentExt = map.getView().calculateExtent(map.getSize())
    const newBookmark = {
      titulo:descripcion,
      extent:currentExt
    }
    store.dispatch(guardarNuevoBookmark(newBookmark))

  }
  onCerrarDialogoBookmarks = () => {
    store.dispatch(openFormBookmarks())
  }
  render(){
    const dialogoBookmarksClass = classNames({
      'dialog-open':this.props.isOpen,
      'dialog-close':true
    })



    return (
      <div className={dialogoBookmarksClass}>
        <AppBar
          showMenuIconButton={false}
          className='app-bar'
          titleStyle={{fontSize:'18px'}}
          title="Bookmarks"
          iconElementRight={<IconButton><IconCerrar/></IconButton>}
          onRightIconButtonTouchTap={this.onCerrarDialogoBookmarks}
          />
        <BookmarksList
          onClickIrBookmark={this.onClickIrBookmark}
          onClickBorrarBookmark = {this.onClickBorrarBookmark}
          onClickEditarBookmark = {this.onClickEditarBookmark}
          list={this.props.bookmarks} />
        <Divider />
      <CrearBookmark
      onSubmit={this.onSubmitNuevoBookmark}/>

      </div>
    )
  }
}

export default Bookmarks
