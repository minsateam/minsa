import React from 'react'
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader'
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {grey400, darkBlack, lightBlack,lightBlue300} from 'material-ui/styles/colors';
import './BookmarksList.css'
import IconPlace from 'material-ui/svg-icons/maps/place'

const iconButtonElement = (
  <IconButton
    touch={true}
    tooltip="more"
    tooltipPosition="bottom-left"
  >
    <MoreVertIcon color={grey400} />
  </IconButton>
);


const style = {
  height:'250px',
  overflow:'scroll'
}


const BookmarksList = (props) => {
  const handleClickIrBookmark = (bookmark) => {
    props.onClickIrBookmark(bookmark)
  }
  const handleClickBorrarBookmark = (bookmark) => {
    props.onClickBorrarBookmark(bookmark)
  }
  const handleClickEditarBookmark = (bookmark) => {
    props.onClickEditarBookmark(bookmark)
  }
  const rightIconMenu = (bookmark)=>(
      <IconMenu iconButtonElement={iconButtonElement}>
      <MenuItem onTouchTap={() => handleClickIrBookmark(bookmark)}>Ir</MenuItem>
      <MenuItem onTouchTap={() => handleClickEditarBookmark(bookmark)}>Editar</MenuItem>
      <MenuItem onTouchTap={() => handleClickBorrarBookmark(bookmark)}>Borrar</MenuItem>
      </IconMenu>
  )
  const bookmarksListOrederByCreatedAt = props.list.sort((a,b) => {
    return b.createdAt-a.createdAt
  })
  return (
    <div>
      <List style = {style}>
        <Subheader>Mis Bookmarks</Subheader>
      {bookmarksListOrederByCreatedAt.map((bookmark) =>
        <ListItem
          onTouchTap={() => handleClickIrBookmark(bookmark)}
          key={bookmark.id}
          primaryText={bookmark.get('titulo')}
          rightIconButton={rightIconMenu(bookmark)}
          leftIcon={<IconPlace color={lightBlue300} />}
        />
      )}
      </List>
    </div>

  )
}

export default BookmarksList
