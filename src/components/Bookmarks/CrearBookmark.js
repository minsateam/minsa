import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField';
import {orange500, blue500} from 'material-ui/styles/colors'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import {css} from 'glamor'
const styles = {
  errorStyle: {
    color: orange500,
  },
  underlineStyle: {
    borderColor: orange500,
  },
  floatingLabelStyle: {
    color: orange500,
    fontSize:'12px'
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
};
const rule = css({
  textAlign:'center'
})
const CrearBookmark = (props) => {

  const handleSubmit = (evt) => {
    evt.preventDefault()
    props.onSubmit(evt.target.bookmarkDescription.value)
    evt.target.bookmarkDescription.value = ''
  }
  return (
    <div {...rule}>
      <form onSubmit={handleSubmit}>
        <TextField
          floatingLabelText="Nuevo bookmark"
          floatingLabelStyle={styles.floatingLabelStyle}
          floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
          fullWidth={true}
          name='bookmarkDescription'
        />
        <FloatingActionButton
            type='submit'
            mini={true}  >
            <ContentAdd />
        </FloatingActionButton>
      </form>

    </div>

  )
}
export default CrearBookmark
