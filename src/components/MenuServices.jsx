import React from 'react';
import { css } from 'glamor'

class MenuServices extends React.Component {
  constructor() {
    super();
    this.state = { vermenu: true };
  }
  onDesplegarMenu(e) {
    e.preventDefault()
    this.setState({
      vermenu: !this.state.vermenu
    })
  }
  renderSubMenu(l){
    if(l.Layer){
      return (
        <ul>
          {l.Layer.map((sub)=>{
            return (
              <li key={sub.Name}>
                <a href="#" data-name={sub.Name} onClick={this.onClickItemMenuServices.bind(this)}>{sub.Title}</a>
                {this.renderSubMenu(sub)}
              </li>
            )
          })}
        </ul>
      )

    }else{
      return null
    }

  }
  onClickItemMenuServices=(e)=>{
    const layername = e.target.dataset.name
    const layertitulo = e.target.innerHTML
    this.props.onClickItemMenuServices(layername,layertitulo)
  }
  renderMenu() {
    const capability = this.props.capability
    const layers = capability.filter((layer) => {
      if (layer.Attribution) {
        return layer
      }
    })

    return layers.map((layer) => {
      return (
        <li key={layer.Name}>
          <a href="#" data-name={layer.Name} data-titulo={layer.Title} onClick={()=>this.onClickItemMenuServices}>{layer.Title}</a>
            {this.renderSubMenu(layer)}
        </li>
      )
    })


  }



  render() {
    const renderMenu = this.renderMenu()
    const rule = css({
      display: "inline-block",

      ' a': {
        color: "red",
        fontWeigth: "bold"

      },
      ' a:hover': {
        color: "blue"

      },
      ' ul': {
        listStyle: "none",
        position: "absolute",
        background: "#f5f5f5",
        border: "1px solid #ccc",
        borderBottomLeftRadius: "8px",
        borderBottomRightRadius: "8px",
        borderTop: "none",
        padding: "20px 0",
        maxWidth: "200px",
        zIndex: 10
      },
      ' ul > li:hover': {
        background: "white"
      },
      ' li a': {
        color: "black",
        display: "block",
        padding: "10px 20px"
      },
      ' ul ul': {
        border: "1px solid #ccc",
        borderRight: "none",
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        padding: 0,
        left: "100%",
        top: "-1px",
        visibility: "hidden"
      },
      ' ul li': {
        position: "relative"
      },
      ' ul > li:first-child': {
        borderTopLeftRadius: "20px"
      },
      ' li:hover > ul': {
        visibility: "visible"
      }
    })
    return (
      <nav {...rule}>
        <a href="#" title="Servicios" onClick={this.onDesplegarMenu.bind(this)}>Menu de Datos</a>
        <ul hidden={this.state.vermenu}>
          {renderMenu}
        </ul>
      </nav>
    )
  }
}

export default MenuServices;

/*
{layer.Layer.map((l) => {
              return (
                <li key={l.Name}>
                  <a href="#" data-name={l.Name} onClick={this.onClickItemMenuServices.bind(this)}>{l.Title}</a>
                  <ul>
                    {l.Layer.map((sl) => {
                      return (
                        <li key={sl.Name}>
                          <a href="#" data-name={sl.Name} onClick={this.onClickItemMenuServices.bind(this)}>{sl.Title}</a>
                          {this.renderSubMenu(sl)}
                        </li>

                      )
                    })}
                  </ul>
                </li>
              )
            })}
*/
