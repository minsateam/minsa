import React from 'react'
import { css } from 'glamor'
import ol from 'openlayers'
import axios from 'axios'
import FileSaver from 'file-saver'


import { Dropdown, Menu, Icon, Divider } from 'semantic-ui-react'
const Exportar = ({ layer, map, spinerOn, spinerOff, snackBarOn }) => {
    const url = "http://gisnovomap.com/geoserver/wfs"

    const makeFile = (data) => {
        const file = new File([JSON.stringify(data)], "dataset.geojson", { type: "text/json;charset=utf-8" });
        FileSaver.saveAs(file)
        spinerOff()
        snackBarOn("Archivo descargado.")
    }
    const exoportToGeoJson = () => {
        spinerOn()
        const source = layer.getSource()
        switch (layer.get('origen')) {
            case "wms":
                const filters = source.getParams()['cql_filter']

                const params = {
                    service: "wfs",
                    version: "1.3.0",
                    request: "GetFeature",
                    typeName: layer.get('nombre'),
                    srsName: "EPSG:4326",
                    outputFormat: "application/json"
                }
                if (filters) {
                    params['cql_filter'] = filters
                }
                axios.get(url, {
                    params: params
                })
                    .then((result) => {
                        makeFile(result.data)

                    })
                    .catch((err) => {
                        snackBarOn("Error en la descarga del archivo " + err.message)
                    })

                break
            default:
                const jsonFormat = new ol.format.GeoJSON()
                const jsonData = jsonFormat.writeFeaturesObject(source.getFeatures(), {
                    dataProjection: "EPSG:4326",
                    featureProjection: "EPSG:3857"
                })
                makeFile(jsonData)
                break


        }

    }

    const exportToCsv = () => {
        spinerOn()
        const filters = layer.getSource().getParams()['cql_filter']

        const params = {
            service: "wfs",
            version: "1.3.0",
            request: "GetFeature",
            typeName: layer.get('nombre'),
            srsName: "EPSG:4326",
            outputFormat: "csv"
        }
        if (filters) {
            params['cql_filter'] = filters
        }
        axios.get(url, {
            params: params
        })
            .then((result) => {
                const file = new File([result.data], "dataset.csv", { type: "text/csv;charset=utf-8" })
                FileSaver.saveAs(file)
                spinerOff()
                snackBarOn("Archivo descargado.")

            })
            .catch((err) => {
                spinerOff()
                snackBarOn("Error en la descarga del archivo " + err.message)
            })
    }
    const rule = css({
        transform: "translateX(-180px)"

    })
    return (

        <Dropdown item text='Exportar..'>
            <Dropdown.Menu className={`${rule}`}>
                <Dropdown.Item
                    onClick={exoportToGeoJson}
                    //active={this.state.interactiveState}

                    text="GeoJson"
                />

                {layer.get('tipo') ==="imgservice" &&
                <Dropdown.Item
                    onClick={exportToCsv}>
                    CSV
                </Dropdown.Item>}
                <Divider />

            </Dropdown.Menu>

        </Dropdown>


    )
}

export default Exportar
