import React from 'react';
import { Checkbox, Segment } from 'semantic-ui-react'

class Visible extends React.Component {
  constructor(props) {
    super(props);
   
  }
  onChange(e,d){
    
    this.props.onChange()

  }

  render() {
    return (
  
        <Checkbox toggle label={this.props.titulo} onChange={this.onChange.bind(this)} checked={this.props.visible}/>

  
    )
  }

 
}

export default Visible;
