import React from 'react';
import {Button,Popup,Grid,Header} from 'semantic-ui-react'
import Editor from './BotonesWFS/Editor.jsx'
import store from '../../store/store'
import StylesEditor from './BotonesWFS/StylesEditor.jsx'
import MyLabelEditor from './BotonesWFS/MyLabelEditor.jsx'
import HeatMapLayer from './BotonesWFS/HeatMapLayer/HeatMapLayer.jsx'



class BotonesWFS extends React.Component {
    constructor() {
        super();
        this.state = {
          labelEditorIsOpen: false,
          heatMapEditorIsOpen:false,
          style:{}
         };
    }
    handleOpenLabel = ()=>{
        this.setState({
            labelEditorIsOpen:!this.state.labelEditorIsOpen
        })


    }
    handleOpenHeatMap = () => {
      this.setState({
        heatMapEditorIsOpen:!this.state.heatMapEditorIsOpen
      });
    }
    getAtributos = ()=>{
        const objetoDeAtributos = this.props.layer.get('headers')
        let arrayDeAtributos = []
        for(const nombreDeAtributo in objetoDeAtributos){
            arrayDeAtributos.push(nombreDeAtributo)
        }
        return arrayDeAtributos
    }
    
    onChangeStyle = (style) => {
      this.setState({
        style:style
      });
    }



    render() {
        const selectedFeatures = store.getState().selectedFeatures
        const atributos = this.getAtributos()
        const geomType = this.props.geomType
   
        return (
            <Button.Group>
                <Editor
                  layer = {this.props.layer}
                  map = {this.props.map}
                  drawNewElement = {this.props.drawNewElement}
                  snapI={this.props.snapI}
                  modifyI={this.props.modifyI}
                  selectInteraction={this.props.selectInteraction}

                 />

                <StylesEditor
                  layer={this.props.layer}
                  onChangeStyle={this.onChangeStyle}
                  isOpen={true}
                />
                <MyLabelEditor
                  atributos={atributos}
                  layer={this.props.layer}
                  isOpen={this.state.labelEditorIsOpen}
                  handleOpen={this.handleOpenLabel}
                  currentStyle={this.state.style}
                 />
                 {geomType == "Point" && <HeatMapLayer
                   atributos={atributos}
                   layer={this.props.layer}
                   isOpen={this.state.heatMapEditorIsOpen}
                   handleOpen={this.handleOpenHeatMap}
                   map = {this.props.map}
                  />}
                
                  


            </Button.Group>
        )
    }
    componentDidMount() {
        store.subscribe(()=>this.forceUpdate())
    }



}

export default BotonesWFS;
