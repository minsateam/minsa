import React, {Component} from 'react'
import {Dropdown,Button} from 'semantic-ui-react'
import Dialog from 'material-ui/Dialog'
 //Material UI:
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import swal from 'sweetalert'

import './QueryLayer.css'


class QueryLayer extends Component {
    state = {
        campo:null,
        operador:null,
        logico:null,
        expresion:""
    }

    onSubmit = (e) =>{
        e.preventDefault()
        const expresion = e.target.expresion.value
        const params = this.props.layer.getSource().getParams()
        if(expresion){
          const newparams = {...params,cql_filter:expresion}
          this.props.layer.getSource().updateParams(newparams)
          this.props.onSetCql_filters(newparams) //envia los parametros a Layer.jsx
        }else{
          swal({
            title: "Parámetros de filtrado en blanco",
            text: "Retornando todos los valores.",
            timer: 2000,
            showConfirmButton: false
          })
          delete params['cql_filter']
          this.props.layer.getSource().updateParams(params)
          this.props.onSetCql_filters(params) //envia los parametros a Layer.jsx
        }





    }

    handleChangeProp = (e,d,campo)=>{
        const expresion = this.state.expresion + " "+campo
        this.setState({
            campo:campo,
            expresion:expresion

            })
    }
    renderOptionsItem = () => {
        const arrayDePropiedades = []
        const headers = this.props.layer.get('headers')
        for(const propiedad in headers){
            arrayDePropiedades.push({
                propname:propiedad,
                tipo:headers[propiedad]
            })
        }
        const optionsItems = arrayDePropiedades.map((optionItem)=>{
            return (
                <MenuItem key={optionItem.propname} value={optionItem.propname}
                primaryText={optionItem.propname}
                secondaryText={optionItem.tipo}
                />
            )
        })
        return optionsItems
    }
    handleChangeOperador=(e,d,operador)=>{
        const expresion = this.state.expresion + " "+operador
        this.setState({
            operador:operador,
            expresion:expresion
        })

    }
    onChangeExpresion = (e)=>{

        this.setState({
            expresion:e.target.value
        })

    }
    handleChangeLogico = (e,d,logico)=>{
        const expresion = this.state.expresion + " " + logico
        this.setState({
            logico:logico,
            expresion:expresion
        })
    }
    renderOperadorComparativo = ()=>{
        return [
            {
                valor:"=",
                text:"IGUAL QUE",
                key:"1"
            },
            {
                valor:"!=",
                text:"NO ES IGUAL QUE",
                key:"2"
            },
            {
                valor:"<>",
                text:"DISTINTO DE",
                key:"3"
            },
            {
                valor:">",
                text:"MAYOR QUE",
                key:"4"
            },
            {
                valor:">=",
                text:"MAYOR O IGUAL QUE",
                key:"6"
            },
            {
                valor:"<",
                text:"MENOR QUE",
                key:"7"
            },
            {
                valor:"<=",
                text:"MENOR O IGUAL QUE",
                key:"8"
            },
            {
                valor:"BETWEEN",
                text:"ENTRE",
                key:"9"
            },
            {
                valor:"ILIKE",
                text:"LIKE",
                key:"10"
            },
            {
                valor:"IN",
                text:"IN",
                key:"11"
            },
            {
                valor:"IS NULL",
                text:"IS NULL",
                key:"12"
            },
            {
                valor:"IS NOT NULL",
                text:"IS NOT NULL",
                key:"13"
            }
        ]
    }
    renderOperadoresLogicos =()=>{
        return [
            {
                text:"O",
                valor:"OR",
                key:"OR"
            },
            {
                text:"Y",
                valor:"AND",
                key:"AND"
            },
            {
                text:"NOT",
                valor:"NOT",
                key:"NOT"
            }
        ]
    }
    render(){
        const headers = this.props.layer.get('headers')
        const optionItems = this.renderOptionsItem()
        const renderOperadorComparativo = this.renderOperadorComparativo()
        const renderOperadoresLogicos = this.renderOperadoresLogicos()
             return (
                <div className='query-layer-container'>
                    <form onSubmit={this.onSubmit}>
                        <SelectField
                            floatingLabelText="Campos"
                            value={this.state.campo}
                            onChange={this.handleChangeProp}
                            >

                            {optionItems}
                        </SelectField>
                        <SelectField
                            floatingLabelText="operador"
                            value={this.state.operador}
                            onChange={this.handleChangeOperador}
                            >
                            {renderOperadorComparativo.map((operador)=>(
                                <MenuItem value={operador.valor} primaryText={operador.text} key={operador.key}/>
                            ))}

                        </SelectField>
                        <SelectField
                            floatingLabelText="logico"
                            value={this.state.logico}
                            onChange={this.handleChangeLogico}
                        >
                            {renderOperadoresLogicos.map((logico)=>(
                                <MenuItem value={logico.valor} primaryText = {logico.text} key={logico.key}/>
                            ))}
                        </SelectField>
                        <br/>
                        <TextField
                            floatingLabelText="Expresión"
                            fullWidth
                            multiLine={true}
                            value={this.state.expresion}
                            rows={2}
                            rowsMax={4}
                            name='expresion'
                            textareaStyle={{fontStyle:"italic",fontWeight:"bold",fontSize:"16px",color:"#000099"}}
                            onChange={this.onChangeExpresion}
                        />

                        <br/>
                        <RaisedButton label="Aceptar"
                        type='submit'
                        />

                    </form>


                </div>

             );
    }

}

export default QueryLayer;
