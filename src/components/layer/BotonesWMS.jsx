import React from 'react';
import {Button,Popup,Grid,Header} from 'semantic-ui-react'

class BotonesWMS extends React.Component {
    constructor() {
        super();
        this.state = { someKey: 'someValue' };
    }

    render() {
        return (
            <Button.Group>
                <Popup
                    trigger={<Button icon="object group"></Button>}
                    flowing
                    hoverable
                >
                    <Grid centered divided columns={2}>
                        <Grid.Column textAlign='center'>
                            <Header as='h4'>BBox</Header>
                            <p>Solicitar vectores de este servicio mediante la extension actual del mapa</p>
                            <Button onClick={this.props.onSolicitarVectores}>Solicitar</Button>
                        </Grid.Column>
                        {this.props.cql_filter &&
                            <Grid.Column textAlign='center'>
                                <Header as='h4'>Elementos Visibles</Header>
                                <p>Solicitar vectores de este servicio que esten visibles acorde a una consulta ejecutada previamente.</p>
                                <Button 
                                onClick={()=>this.props.onSolicitarVectoresPorFilter(this.props.cql_filter)} 
                                 >Solicitar</Button>
                                
                            </Grid.Column>
                        }
                        
                    </Grid>
                </Popup>
                <Button onClick={this.props.verLeyenda} icon='play' content="Leyenda"/>

            </Button.Group>
        )
    }


}

export default BotonesWMS;
