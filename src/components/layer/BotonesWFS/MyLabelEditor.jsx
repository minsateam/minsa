import React from 'react'
import LabelModal from 'boundless-sdk/components/LabelModal'
import LabelEditor from 'boundless-sdk/components/LabelEditor'
import {css} from 'glamor'
import { Button } from 'semantic-ui-react'
import ol from 'openlayers'
import FlatButton from 'material-ui/FlatButton'
import classNames from 'classnames'
import './MyLabelEditor.css'
import AppBar from 'material-ui/AppBar'
import IconCerrar from 'material-ui/svg-icons/navigation/close'
import IconButton from 'material-ui/IconButton';

const MyLabelEditor = ({layer,atributos,isOpen,handleOpen,currentStyle})=>{
    const dialogLabelClass = classNames({
      'dialog-label-open':isOpen,
      'dialog-label-close':true
    })


        const onChange = (propiedadesDeTexto)=>{

          const geomType = layer.getSource().getFeatures()[0].getGeometry().getType()
          let labelXOffset,labelYOffset
          if(geomType === "Polygon" || geomType === "MultiPolygon"){
            labelXOffset=0;
            labelYOffset=0;
          }else{
            labelXOffset=20;
            labelYOffset=17;
          }
            const actualStyle = (feature,res) =>{
              const objectText = {
                text:feature.get(propiedadesDeTexto.labelAttribute),
                font:`${propiedadesDeTexto.fontSize}px sans-serif`,//"14px sans-serif",
                offsetX:20,
                offsetY:-20,
                fill:new ol.style.Fill({color:propiedadesDeTexto.fontColor.hex})
              }
              const label = {
                label:propiedadesDeTexto.labelAttribute,
                fontFamily:'sans-serif',
                fontSize:`${propiedadesDeTexto.fontSize}px`,
                labelXOffset:labelXOffset,
                labelYOffset:labelYOffset,
                fontColor:propiedadesDeTexto.fontColor.hex
              }
              feature.set('label',label.label)
              feature.set('fontFamily',label.fontFamily)
              feature.set('fontSize',label.fontSize)
              feature.set('labelXOffset',label.labelXOffset)
              feature.set('labelYOffset',label.labelYOffset)
              feature.set('fontColor',label.fontColor)
              feature.set('fillColor',currentStyle.getFill().getColor())
              feature.set('strokeColor',currentStyle.getStroke().getColor())
              feature.set('strokeWidth',currentStyle.getStroke().getWidth())
              feature.set('pointRadius',currentStyle.getImage().getRadius())


              const newStyle = new ol.style.Style({
                      fill:currentStyle.getFill(),
                      stroke:currentStyle.getStroke(),
                      image:currentStyle.getImage(),
                      text:new ol.style.Text(objectText)
              })
              return newStyle
            }
            layer.setStyle(actualStyle)


            const hacerStyleConTexto = (elementoEspacial,resolucion)=>{

                const textoStyle = new ol.style.Text({
                    text:elementoEspacial.get(propiedadesDeTexto.labelAttribute),
                    font:`${propiedadesDeTexto.fontSize}px sans-serif`,//"14px sans-serif",
                    offsetX:20,
                    offsetY:-20,
                    fill:new ol.style.Fill({color:propiedadesDeTexto.fontColor.hex})
                })
                elementoEspacial.setStyle(currentStyle)

            }


        }
    return (
        <div>
            <Button icon="tag"
                active={isOpen}
                onClick={handleOpen}
            />
            <div className={dialogLabelClass}>
              <AppBar
                showMenuIconButton={false}
                titleStyle={{fontSize:'18px'}}
                title="Editor de Etiquetas"
                iconElementRight={<IconButton><IconCerrar/></IconButton>}
                onRightIconButtonTouchTap={handleOpen}
                />

             <LabelEditor attributes={atributos} onChange={onChange} />
             <FlatButton label="Aceptar" onTouchTap={handleOpen} primary={true}/>
            </div>
        </div>
    )
}

export default MyLabelEditor
