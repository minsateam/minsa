import React from 'react';
import { Popup, Grid, Header, Button } from 'semantic-ui-react'
import { NotificationManager } from 'react-notifications'
import ol from 'openlayers'
import Parse from 'parse'
import store from '../../../store/store'
import { changeLayers } from '../../../actions/mapActions'
import { isEditing } from '../../../actions/appActions'
import {updateFeaturesToGeoserver,validarUserEdit,saveElementToParse} from '../../../services/geoprocesos'

class Editor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nuevoElementoStatus: {
        status: false,
        text: "Iniciar"
      },
      modifyElementoStatus:{
        status:false,
        text:"Iniciar"
      }
    };
    this.onNuevoElemento = this.onNuevoElemento.bind(this)
    this.onModificarElemento = this.onModificarElemento.bind(this)

    const sourceVectorLayer = props.layer.getSource()
    let featureCollectionToModify = new ol.Collection()
    this.selectInteraction = store.getState().map.selectInteraction
    store.subscribe(()=>{
      featureCollectionToModify.clear()
      featureCollectionToModify.extend(store.getState().map.selectedFeatures)
    })


    this.modifyI = new ol.interaction.Modify({
      features: featureCollectionToModify
    })
    this.modifyI.setActive(false)
    this.modifyI.on('modifyend',(evt)=>{
      if(props.layer.get('origen')==="wfs"){
        validarUserEdit(evt.features.getArray(),props.layer.get('nombreservicio'),'editfeatures')
      }else{
        saveElementToParse(props.layer.get('id'),props.layer)
      }
      
      //updateFeaturesToGeoserver(evt.features.getArray(),this.props.layer.get('nombreservicio'))
    })

    props.map.addInteraction(this.modifyI)
  }

  onNuevoElemento() {

    const snapI = this.props.snapI
    const statusEdicionLayer = this.props.layer.get('statusEdicion')
    if(this.modifyI.getActive()){
      this.modifyI.setActive(false)
      snapI.setActive(false)
      this.setState({
        modifyElementoStatus: {
        status:false
      }
      })
    }
    if (statusEdicionLayer) {
      NotificationManager.info("Desactivando modo de Edicion de la capa " + this.props.layer.get('nombre'))

    } else {
      NotificationManager.success("Activando modo de Edicion de la capa " + this.props.layer.get('nombre'))

    }
    const drawNewElement = this.props.drawNewElement

    this.props.layer.set('statusEdicion', !drawNewElement.getActive())
    store.dispatch(changeLayers())
    store.dispatch(isEditing(
      {
        status:!drawNewElement.getActive(),
        layer:this.props.layer
      }

      ))

    snapI.setActive(!snapI.getActive())
    drawNewElement.setActive(!drawNewElement.getActive())

    this.setState({
      nuevoElementoStatus: {
        status: !this.state.nuevoElementoStatus.status
      }
    })
  }
  onModificarElemento() {
    const statusEdicionLayer = this.props.layer.get('statusEdicion')
    const nuevoI = this.props.drawNewElement
    const selectI = this.props.selectInteraction
    const snapI = this.props.snapI
    if (nuevoI.getActive()) {
      nuevoI.setActive(false)
      snapI.setActive(false)
      this.setState({
        nuevoElementoStatus: {
        status:false
      }
      })
    }
    this.props.layer.set('statusEdicion',!this.modifyI.getActive())
    store.dispatch(isEditing({
        status:!this.modifyI.getActive(),
        layer:this.props.layer
      }))
    snapI.setActive(!snapI.getActive())
    selectI.setActive(!selectI.getActive())
    this.modifyI.setActive(!this.modifyI.getActive())

    store.dispatch(changeLayers())
    this.setState({
      modifyElementoStatus:{
        status:!this.state.modifyElementoStatus.status
      }
    })

  }
  statusButtonNuevo() {
    return this.state.nuevoElementoStatus.status ? "Parar" : "Iniciar"
  }
  statusButtonModify(){
    return this.state.modifyElementoStatus.status ? "Parar" : "Iniciar"
  }
  render() {
    const textButtonNuevo = this.statusButtonNuevo()
    const textButtonModify = this.statusButtonModify()
    return (
      <Popup
        trigger={<Button icon="write"></Button>}
        flowing
        hoverable
      >
        <Grid centered divided columns={2}>
          <Grid.Column textAlign='center'>

            <Button
            active={this.state.nuevoElementoStatus.status}
            icon="pencil" onClick={this.onNuevoElemento}
            content={textButtonNuevo}
            label="Nuevo" />
          </Grid.Column>
          <Grid.Column textAlign='center' >

            <Button icon="pencil"
            active={this.state.modifyElementoStatus.status}
              content={textButtonModify}
              label="Modificar"
              onClick={this.onModificarElemento}
            />
          </Grid.Column>


        </Grid>
      </Popup>
    )
  }
  componentDidMount() {

  }
  componentWillUnmount(){
    this.props.map.removeInteraction(this.modifyI)
  }
}

export default Editor;
