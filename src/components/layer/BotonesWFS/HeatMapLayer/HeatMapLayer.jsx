import React from 'react'
import { Button } from 'semantic-ui-react'
import {crearHeatLayer} from '../../../../services/geoprocesos'
//Material
import FlatButton from 'material-ui/FlatButton'
import AppBar from 'material-ui/AppBar'
import IconCerrar from 'material-ui/svg-icons/navigation/close'
import IconButton from 'material-ui/IconButton';
import classNames from 'classnames'
import './HeatMapLayer.css'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'


class HeatMapLayer extends React.Component{
  state = {
    weight:this.props.atributos[0]
  }
  handleOpen = () => {
    this.props.handleOpen()
  }
  handleAceptar = () => {
    this.crearHeatLayer()
    this.props.handleOpen()
  }
  handleChangeAtributo = (evt,index,valor) => {
    this.setState({
      weight:valor
    });
  }
  crearHeatLayer = () => {
    const map = this.props.map
    const weight = this.state.weight
    const heatLayer = crearHeatLayer(weight,this.props.layer)
    map.addLayer(heatLayer)
  }
  render(){
      const dialogHeatmapclass = classNames({
        'dialog-heatmap-open':this.props.isOpen,
        'dialog-heatmap-close':true
      })


    return (
      <div>
        <Button icon='fire'
          active={this.props.isOpen}
          onClick={this.handleOpen}
        />
          <div className={dialogHeatmapclass}>
            <AppBar
              titleStyle={{fontSize:'18px'}}
              showMenuIconButton={false}
              title="Mapa de Calor"
              iconElementRight={<IconButton><IconCerrar/></IconButton>}
              onRightIconButtonTouchTap={this.handleOpen}
              />

              <SelectField
                floatingLabelText="Valoracion"
                value={this.state.weight}
                onChange={this.handleChangeAtributo}
              >
                {this.props.atributos.map((atributo) => (
                  <MenuItem
                    key={atributo}
                    value={atributo} primaryText={atributo}/>
                ))}

            </SelectField>
             <FlatButton label="Aceptar" onTouchTap={this.handleAceptar} labelStyle={{color:'white'}}/>
          </div>

      </div>
    )
  }
}





export default HeatMapLayer
