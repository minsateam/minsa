import React from 'react';
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import {css} from 'glamor'


class BorrarElemento extends React.Component {
    constructor() {
        super();
        this.state = { onAbrirDialogo: false };
    }

    render() {
        const rule = css({
            display:'inline',
            marginRight:"5px"
        })
        return (
            <div {...rule}>
                <RaisedButton
                    secondary={true}
                    icon={<i class="fa fa-trash-o" aria-hidden="true"></i>}
                    type='button'
                    label="Borrar"
                    onTouchTap={()=>this.props.onAbrirDialogo()}
                />
                <Dialog
                    actions={this.props.actions}
                    modal={true}
                    open={this.props.open}
                    //onRequestClose={this.handleClose}
                >
                    Borrar elemento?
                </Dialog>

            </div>
        )
    }

   
}

export default BorrarElemento;
