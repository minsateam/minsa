import React from 'react';
import ReactDOM from 'react-dom'
import { Button, Form, Checkbox, Header, Card, Icon } from 'semantic-ui-react'
import Draggable from 'react-draggable'
import { css } from 'glamor'
import store from '../../../store/store'
import * as layerActions from '../../../actions/layerActions'
import RaisedButton from 'material-ui/RaisedButton'
import FontIcon from 'material-ui/FontIcon'
import ol from 'openlayers'
import DatePicker from 'material-ui/DatePicker'
import { updateFeaturesToGeoserver,deleteFeaturesToGeoserver,
    validarUserEdit,saveElementToParse } from '../../../services/geoprocesos'
//actions:
import {removeSelectedFeature} from '../../../actions/mapActions'
import { toggleAtributes } from '../../../actions/layerActions'

import moment from 'moment'
import { NotificationManager } from 'react-notifications'
import BorrarElemento from './BorrarElemento.jsx'
import FlatButton from 'material-ui/FlatButton'
import classNames from 'classnames'
import './AtributosForm.css'

class AtributosForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {},
            openDialogConfirmBorrar: false
        }


    }
    componentWillReceiveProps(props) {
        let headers
        if (props.isEditing.layer) {
            headers = props.isEditing.layer.get('headers')

        }

        const feature = props.features[0]
        const formData = {}
        for (const p in headers) {
            switch (headers[p]) {
                case "string":
                    if (feature) {
                        if (feature.get(p)) {
                            formData[p] = feature.get(p)

                        } else {
                            formData[p] = ""
                        }
                    }

                    break
                case "number":
                    if (feature) {
                        if (feature.get(p)) {
                            formData[p] = feature.get(p)
                        } else {
                            formData[p] = 0
                        }
                    }

                    break
                case "date-time":
                    if (feature) {
                        if (feature.get(p)) {

                            formData[p] = new Date(feature.get(p))
                        } else {
                            formData[p] = ""
                        }
                    }

                    break
            }

        }
        this.setState({
            formData: formData
        })

    }
    onChange(evt, data) {
        const formData = { ...this.state.formData }

        formData[data.name] = data.value
        this.setState({
            formData: formData
        })
    }
    onChangeDate = (evt, data, campo) => {

        const formData = { ...this.state.formData }
        formData[campo] = data
        this.setState({
            formData: formData
        })
    }

    onSubmit(evt, data) {

        const layer = this.props.isEditing.layer
        const featureType = layer.get('nombreservicio')
        evt.preventDefault()
        const feature = this.props.features[0]
        for (const p in data.formData) {
            if (this.state.formData[p] instanceof Date) {
                feature.set(p, this.state.formData[p].toISOString())
            } else {
                feature.set(p, data.formData[p])

            }
        }
        if(layer.get('origen')==="parse"){
            saveElementToParse(layer.get('id'),layer)
        }else{
            updateFeaturesToGeoserver([feature], featureType)
        }
        
        
    }


    renderFields() {
        //const feature = this.props.features[0]
        if (this.props.isEditing.layer) {
            const feature = this.props.features[0]


            const headers = this.props.isEditing.layer.get('headers')
            const arrayFields = []
            for (const p in headers) {
                let field

                switch (headers[p]) {
                    case "string":

                        field = <Form.Input onChange={this.onChange.bind(this)} type="text" label={p} name={p} value={this.state.formData[p]} />
                        arrayFields.push(field)
                        break
                    case "number":

                        field = <Form.Input onChange={this.onChange.bind(this)} type="number" step="0.1" label={p} name={p} value={this.state.formData[p]} />
                        arrayFields.push(field)
                        break
                    case "date-time":

                        field =
                            <DatePicker
                                cancelLabel="Cancelar"
                                onChange={(evt, fecha) => this.onChangeDate(evt, fecha, p)}
                                floatingLabelText={p}
                                container="inline"
                                hintText={p}
                                value={this.state.formData[p]}
                                name={p}
                            />



                        arrayFields.push(field)
                        break
                }
            }
            return arrayFields

        }

    }
    abrirDialogoBorrar() {
        this.setState({
            openDialogConfirmBorrar: true
        })
    }
    cerrarDialogoBorrar() {
        this.setState({
            openDialogConfirmBorrar: false
        })
    }
    onBorrarElemento() {
       
        const features = this.props.features
        const layer = this.props.isEditing.layer
        
        this.cerrarDialogoBorrar()
        if(layer.get('origen')==="parse"){
            layer.getSource().removeFeature(features[0])
            console.log(features[0])
            store.dispatch(removeSelectedFeature(features[0]))
            store.dispatch(toggleAtributes())
            
            saveElementToParse(layer.get('id'),layer)
        }else{
            deleteFeaturesToGeoserver(features,layer,this.props.selectInteraction)
        }
        
        
    }

    render() {
        const formclass = classNames({
          'atributos-form-open':this.props.isAtributeVisible,
          'atributos-form-close':true
        })
        const rule = css({
            position: "absolute",
            top: "140%",
            left: "-45%",
            backgroundColor: "white",
            zIndex: 10,
            visibility: this.props.isAtributeVisible
        })
        const rulefields = css({
            maxHeight: "500px",
            overflow: "scroll"
        })
        const isActiveBoton = this.props.isAtributeVisible == "hidden" ? false : true
        const isDisabled = this.props.features.length == 1 && this.props.isEditing.status
        const renderFields = this.renderFields()
        const accionesBorrar = [
            <FlatButton
                label="Cancelar"
                primary={true}
                onTouchTap={this.cerrarDialogoBorrar.bind(this)}
            />,
            <FlatButton
                label="Confirmar"
                primary={true}
                onTouchTap={this.onBorrarElemento.bind(this)}
            />
        ]

        return (
            <div>
                <RaisedButton disabled={!isDisabled} active={isActiveBoton} icon={<i class="fa fa-list-alt" aria-hidden="true"></i>} onClick={() => { store.dispatch(layerActions.toggleAtributes()) }} label="Atributos" />
                <Draggable>
                    <div className={formclass}>
                        <Card>
                            <Card.Content>
                                <Card.Header as="h3">
                                    <Icon name="layout" />
                                    Formulario de Atributos
                                    <Button onClick={() => { store.dispatch(layerActions.toggleAtributes()) }} size="mini" floated="right" icon="remove" />

                                </Card.Header>
                                <Form onSubmit={this.onSubmit.bind(this)}>
                                    <div {...rulefields}>
                                        {renderFields}
                                    </div>

                                    <Card.Content extra>
                                        <RaisedButton
                                            primary={true}
                                            icon={<i class="fa fa-floppy-o" aria-hidden="true"></i>} type='submit' label="Guardar" />

                                            <BorrarElemento
                                                actions={accionesBorrar}
                                                open={this.state.openDialogConfirmBorrar}
                                                onAbrirDialogo={this.abrirDialogoBorrar.bind(this)}

                                            />
                                    </Card.Content>


                                </Form>

                            </Card.Content>

                        </Card>



                    </div>

                </Draggable>
            </div>

        )
    }


}
AtributosForm.propsTypes = {
    feature: React.PropTypes.object

}
AtributosForm.defaultProps = {
    feature: null
}
export default AtributosForm;
