import React from 'react'
import { Popup, Grid, Header, Button } from 'semantic-ui-react'
import PointSymbolizerEditor from 'boundless-sdk/components/PointSymbolizerEditor'
import PolygonSymbolizerEditor from 'boundless-sdk/components/PolygonSymbolizerEditor'
import LineSymbolizerEditor from 'boundless-sdk/components/LineSymbolizerEditor'
import AppBar from 'material-ui/AppBar'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import {css} from 'glamor'
import ol from 'openlayers'
import styleDefault from '../../../services/styles'
import classNames from 'classnames'
import './StylesEditor.css'
import IconCerrar from 'material-ui/svg-icons/navigation/close'
import IconButton from 'material-ui/IconButton';


class StylesEditor extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            isOpen:false,
            styleState:{
                hasStroke:true,
                strokeColor:{
                    hex:"#CE17F2",
                    rgb:{
                        a:0.8,b:242,g:23,r:206
                        //"rgba(206,23,242,0.8)"
                    }
                },
                strokeWidth:1,
                opacity:1,
                hasFill:true,
                fillColor:{
                    hex:"#A5EC0A",
                    rgb:{
                        a:0.7,g:236,b:10,r:165
                        //rgba(165,236,10,0.7)
                    }
                },
                symbolSize:"7",
                symbolType:"circle",
                rotation:0
            }
        }
    }
    handleClose = ()=>{
        this.setState({isOpen:false})
    }
    handleOpen = ()=>{
        this.setState({isOpen:!this.state.isOpen})
    }
    renderSimbolizerType = ()=>{
        const layer = this.props.layer
        let geomType
        let render
        if(layer.getSource().getFeatures().length>0){
            geomType = layer.getSource().getFeatures()[0].getGeometry().getType()
            switch(geomType){
                case "Point":
                render =
                (<div>
                    <PointSymbolizerEditor
                        onChange={this.onChange} initialState={this.state.styleState} />
                    <FlatButton label="Aceptar" onTouchTap={this.handleOpen}/>
                </div>)
                break
                case "Polygon":
                render =
                (<div>
                    <PolygonSymbolizerEditor
                        onChange={this.onChange} initialState={this.state.styleState} />
                    <FlatButton label="Aceptar" onTouchTap={this.handleOpen}/>

                </div>)
                break
                case "MultiPolygon":
                render =
                (<div>
                    <PolygonSymbolizerEditor
                        onChange={this.onChange} initialState={this.state.styleState} />
                    <FlatButton label="Aceptar" onTouchTap={this.handleOpen}/>

                </div>)
                break
                case "LineString":
                render =
                (<div>
                    <LineSymbolizerEditor
                        onChange={this.onChange} initialState={this.state.styleState} />
                    <FlatButton label="Aceptar" onTouchTap={this.handleOpen}/>

                </div>)
                break
                case "MultiLineString":
                render =
                (<div>
                    <LineSymbolizerEditor
                        onChange={this.onChange} initialState={this.state.styleState} />
                    <FlatButton label="Aceptar" onTouchTap={this.handleOpen}/>

                </div>)
                break


            }
            return render

        }else{
            return null
        }

    }
    onChange = (e)=>{
        this.setState({
            styleState:{...this.state.styleState,...e}
        })
        this.setLayerStyle()

    }
    onCerrarEditorSimbologia = () => {
      this.setState({
        isOpen:false
      })
    }
    setLayerStyle=()=>{


        setTimeout(()=>{

            let fill,stroke
            const stylesState = this.state.styleState

            const fillColor = stylesState.fillColor.rgb ? getRgba(stylesState.fillColor.rgb) : stylesState.fillColor.hex
            const strokeColor = stylesState.strokeColor.rgb ? getRgba(stylesState.strokeColor.rgb) : stylesState.strokeColor.hex

            fill = stylesState.hasFill ? new ol.style.Fill({
                    color:fillColor
            }) : null
            stroke = stylesState.hasStroke ? new ol.style.Stroke({
                color:strokeColor,
                width:stylesState.strokeWidth
            }) : null
           const text = new ol.style.Text({
                text:"",
                font:"14px sans-serif",
                offsetX:20,
                offsetY:-20,
                fill:new ol.style.Fill({color:"#494444"})

            });
            let image
            if(stylesState.externalGraphic){
              image = new ol.style.Icon({
                src:stylesState.externalGraphic
              })
            }else{
              switch(stylesState.symbolType){
                  case "circle":
                  image = new ol.style.Circle({
                      fill:fill,
                      stroke:stroke,
                      radius:stylesState.symbolSize

                  })
                  break
                  case "square":
                  image = new ol.style.RegularShape({
                      fill:fill,
                      points:4,
                      stroke:stroke,
                      radius:stylesState.symbolSize,
                      rotation:stylesState.rotation
                  })
                  break
                  case "triangle":
                  image = new ol.style.RegularShape({
                      fill:fill,
                      points:3,
                      stroke:stroke,
                      radius:stylesState.symbolSize,
                      rotation:stylesState.rotation
                  })
                  break
                  case "star":
                  image = new ol.style.RegularShape({
                      fill:fill,
                      points:5,
                      radius1:parseInt(stylesState.symbolSize)/2,
                      radius2:stylesState.symbolSize,
                      stroke:stroke,
                      radius:stylesState.symbolSize,
                      rotation:stylesState.rotation
                  })
                  break
                  default:
                  image = new ol.style.Circle({
                      fill:fill,
                      stroke:stroke,
                      radius:stylesState.symbolSize
                  })
            }

            }
            const style = new ol.style.Style({
                fill:fill,
                stroke:stroke,
                image:image,
                text:text
            })

            this.props.layer.setStyle(style)
            this.props.onChangeStyle(style)

            },1000)
    }
    render(){
      const dialogStylesClass = classNames({
        'dialog-style-open':this.state.isOpen,
        'dialog-style-close':true
      })

        const renderSimbolizerType = this.renderSimbolizerType()
        return (
         <div>
          <Button icon="paint brush"
          active={this.state.isOpen}
          onClick={this.handleOpen}
          />

          <div className={dialogStylesClass}>
            <AppBar
              showMenuIconButton={false}
              titleStyle={{fontSize:'18px'}}
              title="Editor de Simbología"
              iconElementRight={<IconButton><IconCerrar/></IconButton>}
              onRightIconButtonTouchTap={this.onCerrarEditorSimbologia}
              />
              {renderSimbolizerType}
          </div>
        </div>
    )
    }

}

export default StylesEditor

function getRgba(rgba){
    return `rgba(${rgba.r},${rgba.g},${rgba.b},${rgba.a})`
}
