import React from 'react';
import './InfoWindowFeature.css'
import ol from 'openlayers'


//material ui
import Subheader from 'material-ui/Subheader'
import CircularProgress from 'material-ui/CircularProgress'
import IconButton from 'material-ui/IconButton'
import IconCerrar from 'material-ui/svg-icons/content/clear'

import {
    Table,
    TableBody,
    TableFooter,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table'


const InfoWindowFeature = ({ feature, map,close }) => {
    const props = feature.data.data.features[0].properties
    const headers = Object.keys(props)
    const renderFilas = () => {
        //renderFeature()
        if (feature.reciviendo) {
            return <TableRow>
                <TableRowColumn colSpan="2" style={{ textAlign: "center" }}>
                    <CircularProgress />
                </TableRowColumn>
            </TableRow>
        } else if (props) {
            return headers.map((prop) => {
                if (prop === "ruta_pdf") {
                    return (
                        <TableRow key={prop}>
                            <TableHeaderColumn>{prop}</TableHeaderColumn>
                            <TableRowColumn><a href={props[prop]} target="_blank">{props[prop]}</a></TableRowColumn>
                        </TableRow>
                    )
                } else {
                    return (
                        <TableRow key={prop}>
                            <TableHeaderColumn>{prop}</TableHeaderColumn>
                            <TableRowColumn>{props[prop]}</TableRowColumn>
                        </TableRow>)
                }

            })
        } else if (feature.error) {
            return <div>No se recivieron datos por un error de conexion.</div>
        }

    }
    function renderFeature() {

        const format = new ol.format.GeoJSON()
        const features = format.readFeatures(feature.data.data)
        console.log(features)
        const layer = new ol.layer.Vector({
            map: map,
            source: new ol.source.Vector({
                features: features
            })
        })
        //map.getView().fit(layer.getSource().getExtent(),{duraction:'1s'})
    }

    return (
        <div className="info-window-container">
            <IconButton onTouchTap={()=>close()}><IconCerrar></IconCerrar></IconButton>
            <Subheader>Atributos</Subheader>
            <Table
                height="298px"
                fixedHeader={true}>
                <TableHeader
                    displaySelectAll={false}
                    adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn>Prop.</TableHeaderColumn>
                        <TableHeaderColumn>Valor</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody
                    displayRowCheckbox={false}>
                    {renderFilas()}

                </TableBody>

            </Table>

        </div>
    );
};

export default InfoWindowFeature
