import styleDefault from './styles'
import ol from 'openlayers'
import Parse from 'parse'
import uuidv1 from 'uuid/v1'
import axios from 'axios'
import alertify from 'alertifyjs'
import { spinerOn, spinerOff } from '../actions/mapActions'
import store from '../store/store'
import { NotificationManager } from 'react-notifications'
import { parseString } from 'xml2js'
import { removeSelectedFeature } from '../actions/mapActions'
import {snackBarOn} from '../actions/appActions'
import { addResult,emptySearch } from '../actions/searchActions'
import { toggleAtributes } from '../actions/layerActions'
import swal from 'sweetalert'
import '../../node_modules/sweetalert/dist/sweetalert.css'



export function crearVectorLayer(source, nombreLayer) {
    var geomType;
    const features = source.getFeatures()
    for (var i = 0; i < features.length; i++) {
        features[i].setId(i);
    }
    geomType = features[0].getGeometry().getType();
    var vectorLayer = new ol.layer.Vector({
        nombre: nombreLayer,
        titulo:nombreLayer,
        tipo: "vector",
        origen:"local",
        source: source,
        id: uuidv1(),
        style: [styleDefault()]
    });
    vectorLayer.set('type', geomType.toLowerCase());

    //vectorLayer.buildHeaders();
    //this.layerTree.messages.textContent = 'Capa vectorial añadida.';
    return vectorLayer;
};
export function crearLayerImage(layername,titulo) {

    const url = "http://gisnovomap.com/geoserver/wms"
    const layer = new ol.layer.Image({
        nombre: layername,
        tipo: "imgservice",
        origen:"wms",
        titulo:titulo,
        source: new ol.source.ImageWMS({
            url: url,
            params: {
                'LAYERS': layername
            }
        }),
        id: uuidv1()
    })
    layer.getSource().on('imageloadstart', (evt) => {
        store.dispatch(spinerOn())
    })
    layer.getSource().on('imageloadend', (evt) => {
        store.dispatch(spinerOff())
    })
    solicitarMetaLayerWFS(layername, layer)
    return layer

}
export function solicitarVectoresPorFilter(layername, cql_filter,titulolayer) {
    store.dispatch(spinerOn())
    const url = "http://gisnovomap.com/geoserver/wfs"
    const format = new ol.format.WFS()
    const layer = new ol.layer.Vector({
        nombre: layername + " " + "wfs",
        nombreservicio: layername,
        titulo:`${titulolayer} => ${cql_filter}`,
        tipo: "vector",
        origen:"wfs",
        source: new ol.source.Vector({
            loader(ext, res, proj) {
                const params = {
                    service: "wfs",
                    version: "1.1.0",
                    request: "GetFeature",
                    typeName: layername,
                    srsName: "EPSG:3857",
                    cql_filter:cql_filter
                }
                axios.get(url, {
                    params: params
                })
                    .then((response) => {
                        console.log(response)
                        const features = format.readFeatures(response.data)
                        features.forEach((feature) => {
                          if(feature.get('OBJECTID')){
                            feature.setId(feature.get('OBJECTID'))
                          }else{
                            feature.setId(uuidv1())
                          }
                          feature.setGeometryName('geom')
                        })
                        const numfeatures = features.length
                        this.addFeatures(features)
                        store.dispatch(spinerOff())
                        NotificationManager.success('Se cargaron ' + numfeatures + " elementos al mapa", 'Solicitud de Datos exitosa')


                    })
                    .catch((err) => {
                        tore.dispatch(spinerOff())
                        console.log(err)
                        NotificationManager.error('Se presentó un porblema al cargar los datos. ' + err.message)
                    })
            }

        }),
        id: uuidv1(),
        style:[styleDefault()]
    })
    solicitarMetaLayerWFS(layername, layer)
    return layer

}
export function solicitarVectoresPorDibujo(layername, wkt) {
    store.dispatch(spinerOn())
    const url = "http://gisnovomap.com/geoserver/wfs"
    const format = new ol.format.WFS()
    const layer = new ol.layer.Vector({
        nombre: layername + " " + "wfs",
        nombreservicio: layername,
        tipo: "vector",
        source: new ol.source.Vector({
            loader(ext, res, proj) {
                const params = {
                    service: "wfs",
                    version: "1.1.0",
                    request: "GetFeature",
                    typeName: layername,
                    srsName: "EPSG:3857",
                    cql_filter: "INTERSECTS(geom," + wkt + ")"
                }
                axios.get(url, {
                    params: params
                })
                    .then((response) => {
                        const features = format.readFeatures(response.data)
                        features.forEach((feature) => {
                            feature.setId(uuidv1())
                        })
                        const numfeatures = features.length
                        this.addFeatures(features)
                        store.dispatch(spinerOff())
                        NotificationManager.success('Se cargaron ' + numfeatures + " elementos al mapa", 'Solicitud de Datos exitosa')


                    })
                    .catch((err) => {
                        console.log(err)
                        NotificationManager.error('Se presentó un porblema al cargar los datos. ' + err.message)
                    })
            }

        })
    })
    layer.getSource().on('addfeature', (evt) => {

        evt.feature.setId(uuidv1())
    })
    return layer

}
export function crearVectorServiceByBbox(layer, bbox, res) {
    const layertitulo = layer.get('titulo')
    const nombreservicio = layer.get('nombre')
    store.dispatch(spinerOn())
    const url = "http://gisnovomap.com/geoserver/wfs"
    const format = new ol.format.WFS()
    const layervector = new ol.layer.Vector({
        nombre: layertitulo + " " + "wfs",
        titulo:layertitulo + " " + "wfs",
        nombreservicio: nombreservicio,
        tipo: "vector",
        origen: "wfs",
        source: new ol.source.Vector({

            loader(ext, res, proj) {
                const params = {
                    service: "wfs",
                    version: "1.1.0",
                    request: "GetFeature",
                    typeName: nombreservicio,
                    srsName: "EPSG:3857",
                    bbox: ext.join(',') + ",EPSG:3857"
                }
                axios.get(url, {
                    params: params
                })
                    .then((response) => {

                        const features = format.readFeatures(response.data)

                        features.forEach((feature) => {
                          if(feature.get('OBJECTID')){


                            feature.setId(feature.get('OBJECTID'))
                          }else if(feature.get('gid')){

                            feature.setId(feature.get('gid'))
                          }else{
                              feature.setId(uuidv1())
                          }
                          feature.setGeometryName('geom')
                        })
                        const numfeatures = features.length

                        this.addFeatures(features)

                        store.dispatch(spinerOff())
                        store.dispatch(snackBarOn("Se cargaron "+numfeatures+" elementos al mapa"))
                        //NotificationManager.success('Se cargaron '+numfeatures+" elementos al mapa",'Solicitud de Datos exitosa')

                    })
                    .catch((err) => {
                        console.log(err)
                        NotificationManager.error('Se presentó un porblema al cargar los datos. ' + err.message)
                    })
            },
            strategy: ol.loadingstrategy.bbox
        }),
        id: uuidv1(),
        style:[styleDefault()]
    })
    solicitarMetaLayerWFS(nombreservicio, layervector)
    return layervector
}

export function getPositionForFeature(geom) {
    let position

    switch (geom.getType()) {
        case 'MultiPolygon':
            var poly = geom.getPolygons().reduce(function (left, right) {
                return left.getArea() > right.getArea() ? left : right;
            });
            position = poly.getInteriorPoint().getCoordinates();
            break;
        case 'Polygon':
            position = geom.getInteriorPoint().getCoordinates();
            break;
        case 'LineString':
            position = geom.getCoordinateAt(0.5)
            break
        case 'MultiLineString':
            position = geom.getFirstCoordinate()
            break
        case 'Point':
            position = geom.getCoordinates()
    }
    return position

}

export function solicitarMetaLayerWFS(layername, layer) {

    const url = "http://gisnovomap.com/geoserver/wfs"
    axios.get(url, {
        params: {
            service: 'wfs',
            version: '1.1.0',
            request: 'DescribeFeatureType',
            typeName: layername,
            outputFormat: 'application/json'
        }
    })
        .then((response) => {

            setHeaders(response.data)
        })
        .catch((err) => {
            console.log(err)
        })

    function setHeaders(data) {
        const arrayProperties = data.featureTypes[0].properties
        const headers = {}
        arrayProperties.forEach((property) => {
            headers[property.name] = property.localType
        })
        delete headers.geom
        layer.set('headers', headers)
    }

}
export function validarUserEdit(features,featureType,tarea){

  const elegirTarea = (tarea,valor)=>{
    switch (tarea) {
      case "newfeature":
        saveNewFeatureToGeoserver(features,featureType,valor)
        break;
      case "editfeatures":
        updateFeaturesToGeoserver(features,featureType,valor)
        break;

    }
  }
  let geoserver_pass = ''
    if(sessionStorage.getItem('geoserver_pass')){
      elegirTarea(tarea,sessionStorage.getItem('geoserver_pass'))
    }else{
      NotificationManager.error('No tiene autorizacion todavia')
      swal({
          title:"Contraseña de Edición de Datos",
          text:"Ingresar la contraseña para editar datos. Consulte con el administrador de Geoserver.",
          type:"input",
          inputType:"password",
          showCancelButton:true,
          closeOnConfirm:false,
          animation:"slide-from-top"
      },
      (password)=>{
          switch(password){
              case false:
              sessionStorage.setItem('geoserver_pass',"0")
              elegirTarea(tarea,password)
              break
              case "":
              swal.showInputError("La contraseña no puede quedar en blanco.");
              return false
              break
              default:
              sessionStorage.setItem('geoserver_pass',password)
              swal("Contraseña ingresada", "Validando...");
              elegirTarea(tarea,password)

          }
      }
      )

    }
}
export function saveNewFeatureToGeoserver(feature, featureType,valor) {
      const geoserver_pass = valor
      const geoserver_user = Parse.User.current().get('geoserver_user')
      console.log(geoserver_pass)
      feature.setGeometryName('geom')
      const WFSTSerializer = new ol.format.WFS({
          //schemaLocation:"http://gisnovomap.com/minsa http://gisnovomap.com/geoserver/wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=DescribeFeatureType&TYPENAME=MINSA:CSA_Tuberia&SRSNAME=EPSG:3857&username=MINSA_Admin&password=gj78rt96"
      })
      console.log(featureType)
      const url = "http://gisnovomap.com/geoserver/wfs"
      const featObject = WFSTSerializer.writeTransaction([feature], null, null, {
          featureNS: "http://gisnovomap.com/minsa",
          featureType: featureType,
          featurePrefix: "MINSA",
          geometryName: "geom",
          srsName: "EPSG:3857"
      })
      console.log(featObject)
      axios({
          url: url,
          method: "POST",
          withCredentials:true,
          data: new XMLSerializer().serializeToString(featObject),
          auth:{
            username:geoserver_user,
            password:geoserver_pass
          },
          headers: { 'Content-Type': "text/xml" }
      })
          .then((response) => {
              const result = WFSTSerializer.readTransactionResponse(response.data)
              console.log(result.insertIds[0])
              const idelement = result.insertIds[0].split('.')
              NotificationManager.success("Elemento guardado en el servidor remoto. Id del elemento asignado: " +
                  idelement[1], "Guardado Exitoso")
              feature.set('OBJECTID', idelement[1])
              feature.setId(idelement[1])
          })
          .catch((err) => {
              console.log(err)
              NotificationManager.error('No se pudo guardar el elemento en el servidor. No se asigno id al elemento en el mapa.' + err.message)
              feature.set('OBJECTID', "")
          })


}
export function updateFeaturesToGeoserver(features, featureType,valor) {
  const geoserver_pass = valor
  const geoserver_user = Parse.User.current().get('geoserver_user')
    const WFSTSerializer = new ol.format.WFS({
        //schemaLocation:"http://gisnovomap.com/minsa http://gisnovomap.com/geoserver/wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=DescribeFeatureType&TYPENAME=MINSA:CSA_Tuberia&SRSNAME=EPSG:3857&username=MINSA_Admin&password=gj78rt96"
    })
    const featureTypeCut = featureType.split(':')
    const url = "http://gisnovomap.com/geoserver/wfs"
    const featObject = WFSTSerializer.writeTransaction(null, features, null, {
        featureNS: "http://gisnovomap.com/minsa",
        featureType: featureTypeCut[1],
        featurePrefix: "MINSA",
        geometryName: "geom",
        srsName: "EPSG:3857"
    })
    axios({
        url: url,
        method: "POST",
        data: new XMLSerializer().serializeToString(featObject),
        auth:{
          username:geoserver_user,
          password:geoserver_pass
        },
        headers: { 'Content-Type': "text/xml" }
    })
        .then((response) => {
            const result = WFSTSerializer.readTransactionResponse(response.data)
            if (result.transactionSummary.totalUpdated > 0) {
                //NotificationManager.success("Elemento(s) actualizados en el servidor remoto.")
                store.dispatch(snackBarOn("Elemento(s) actualizados en el servidor remoto."))
            } else {
                NotificationManager.info('No se actualizó ningun elemento')

            }

        })
        .catch((err) => {
            console.log(err)
            NotificationManager.error('No se pudo ejecutar la accion de actualizar en el servidor.' + err.message)
            //feature.set('OBJECTID',"")
        })
}
export function deleteFeaturesToGeoserver(features, layer, selectInteraction) {
    const geoserver_pass = sessionStorage.getItem('geoserver_pass')
    const geoserver_user = Parse.User.current().get('geoserver_user')
    const WFSTSerializer = new ol.format.WFS()
    const featureTypeCut = layer.get('nombreservicio').split(':')
    const url = "http://gisnovomap.com/geoserver/wfs"
    const featObject = WFSTSerializer.writeTransaction(null, null, features, {
        featureNS: "http://gisnovomap.com/minsa",
        featureType: featureTypeCut[1],
        featurePrefix: "MINSA",
        geometryName: "geom",
        srsName: "EPSG:3857"
    })
    axios({
        url: url,
        method: "POST",
        data: new XMLSerializer().serializeToString(featObject),
        auth:{
            username:geoserver_user,
            password:geoserver_pass
        },
        headers: { 'Content-Type': "text/xml" }
    })
        .then((response) => {
            const result = WFSTSerializer.readTransactionResponse(response.data)
            if (result.transactionSummary.totalDeleted > 0) {
                //NotificationManager.success("Elemento(s) borrados en el servidor.")
                store.dispatch(snackBarOn("Elemento(s) borrados en el servidor."))
                layer.getSource().removeFeature(features[0])
                //Notificar a selectedFeatures que debe sacar este elemento
                store.dispatch(removeSelectedFeature(features[0]))
                store.dispatch(toggleAtributes())
                selectInteraction.getFeatures().remove(features[0])


            } else {
                NotificationManager.info('No se actualizó ningun elemento')

            }

        })
        .catch((err) => {
            console.log(err)
            NotificationManager.error('No se pudo ejecutar la accion de actualizar en el servidor.' + err.message)
            //feature.set('OBJECTID',"")
        })
}
export function busqueda(typeName, campo, valor="$%_0044NaN") {
    store.dispatch(spinerOn())
    console.log('iniciando busquda', valor)
    let cql_filter
    if(campo == "OBJECTID" || campo=="DIAMETRO"){
        cql_filter = campo+" = "+valor
    }else{
        cql_filter = campo+" ILIKE '%"+valor+"%'"
    }
    const format = new ol.format.WFS()
    const url = "http://gisnovomap.com/geoserver/wfs"
    const params = {
        service: "wfs",
        version: "1.1.0",
        request: "GetFeature",
        typeName: typeName,
        srsName: "EPSG:3857",
        cql_filter: cql_filter
    }
    axios.get(url, {
        params: params
    })
        .then((response) => {
            const result = format.readFeatures(response.data)

            if (result.length > 0) {
                console.log(cql_filter,typeName,result)
                for (let i = 0; i < result.length; i++) {
                    store.dispatch(addResult(result[i], typeName, valor))

                }

            }else{
                console.log(cql_filter,typeName,response.data)
                console.log("zero results")
            }

            store.dispatch(spinerOff())
        })
        .catch((err) => {
            store.dispatch(spinerOff())
            console.log(err)
        })

}


export function addItemResultToMap(feature) {
    const geom = feature.getGeometry().getType()
    const map = store.getState().map.map
    const servicio = feature.getId().split('.')
    const fill = new ol.style.Fill({
        color: "rgba(255,0,0,0.5)"
    })
    const stroke = new ol.style.Stroke({
        color: "rgba(255,0,0,0.8)",
        width: 1
    })
    const layer = new ol.layer.Vector({
        nombre: "Resultado: " + feature.getId(),
        titulo: "Resultado: "+ feature.getId(),
        nombreservicio: servicio[0],
        tipo: "vector",
        origen: "wfs",
        source: new ol.source.Vector({
            features: [feature]
        }),
        style: [new ol.style.Style({
            image: new ol.style.Circle({
                fill: fill,
                stroke: stroke,
                radius: 6
            }),
            fill: fill,
            stroke:stroke
        })]
    })
    map.addLayer(layer)
    switch(geom){
        case "Point":
        map.getView().animate({
            center:feature.getGeometry().getCoordinates(),
            zoom:16
        })
        break
        default:
        map.getView().fit(feature.getGeometry().getExtent(),{
            duration:1000
        })

    }

}

export function crearHeatLayer(weight,layer){
  const source = layer.getSource()
  const layerName = layer.get('titulo')

  const heatLayer = new ol.layer.Heatmap({
    source:source,
    weight:function(feature){
      return (+feature.get(weight))
    },
    radius:16,
    blur:20

  })
  heatLayer.set('nombre',layerName+"_heatmap")
  heatLayer.set('titulo',layerName+"_heatmap")
  heatLayer.set('tipo','heat')
  heatLayer.set('id',uuidv1())
  return heatLayer
}
export function getLeyendGraphic(layer){
    console.log(layer)
    const url = `http://gisnovomap.com/geoserver/wms?REQUEST=GetLegendGraphic&LAYER=${layer}&FORMAT=image/png&WIDTH=20&HEIGHT=20&LEGEND_OPTIONS=fontSize:16`
    return url

}

export function cargarParseLayer(parseObject){
    const jsonFormat = new ol.format.GeoJSON()
    const layer = new ol.layer.Vector({
        nombre:parseObject.get('tipo'),
        titulo:parseObject.get('tipo'),
        tipo:"vector",
        geometria:parseObject.get('tipo'),
        origen:"parse",
        headers:parseObject.get('headers'),
        source:new ol.source.Vector({
            features:jsonFormat.readFeatures(parseObject.get('data'),{
                dataProjection:"EPSG:4326",
                featureProjection:"EPSG:3857"
            })
        }),
        id: parseObject.id,
        style:[styleDefault()]
    })
    layer.getSource().on('addfeature',(evt)=>{
        evt.feature.setId(uuidv1())
    })
    return layer
}

export function saveElementToParse(id,layer){
        console.log(id)
        const Capas = Parse.Object.extend('Capas')
        const query = new Parse.Query(Capas)
        const jsonFormat = new ol.format.GeoJSON()
        console.log(layer.getSource().getFeatures())
        const jsonData = jsonFormat.writeFeaturesObject(layer.getSource().getFeatures(),{
            dataProjection:"EPSG:4326",
            featureProjection:"EPSG:3857"
        })
        query.get(id,{
            success(capa){
                capa.set('data',jsonData)
                capa.save(null,{
                    success(capa){
                        NotificationManager.success('Data actualizada')
                    },
                    error(err){
                        NotificationManager.error('Problemas con la actualizacion, inténtelo de nuevo.'+err.message)
                    }
                })

            },
            error(err){
                NotificationManager.error("Problemas con la actualizacion "+err.message)
            }
        })
    }
