import ol from 'openlayers'
export default function styleDefault() {
    var fill = new ol.style.Fill({
        color:"rgba(165,236,10,0.7)"
    });
    var stroke= new ol.style.Stroke({
        color:"rgba(206,23,242,0.8)",
        width:1
    });
    var text = new ol.style.Text({
        text:"",
        font:"14px sans-serif",
        offsetX:20,
        offsetY:-20,
        fill:new ol.style.Fill({color:"#494444"})

    });
    var image = new ol.style.Circle({
            fill:fill,
            stroke:stroke,
            radius:7
        })
    var style = new ol.style.Style({
        fill:fill,
        stroke:stroke,
        image:image,
        text:text
    });
    return style;
};